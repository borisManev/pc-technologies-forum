package test.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.reactions.UserCommentReaction;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.CommentRepository;
import com.company.web.forumwebproject.repositories.contracts.UserCommentReactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Set;

import static com.company.web.forumwebproject.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceTests {

    @Mock
    private CommentRepository commentRepository;
    @Mock
    private UserCommentReactionRepository reactionRepository;

    @InjectMocks
    private CommentServiceImpl service;

    private Post post;
    private Comment comment;
    private User user;
    private User diffUser;
    private User admin;
    private User blockedUser;
    private User deletedUser;

    @BeforeEach
    public void beforeEach() {
        user = createMockUser();
        diffUser = getDifferentUser();
        admin = createMockAdmin();
        blockedUser = createMockBlockedUser();
        deletedUser = createMockDeletedUser();

        post = createMockPost();

        comment = createMockComment();
        comment.setCreatedBy(user);
    }

    @Test
    public void search_Should_CallRepository() {
        service.search(Optional.of("keyword"));

        verify(commentRepository).search(Optional.of("keyword"));
    }

    @Test
    public void filter_Should_CallRepository() {
        CommentFilterOptions filterOptions = new CommentFilterOptions();
        service.filter(filterOptions);

        verify(commentRepository).filter(filterOptions);
    }

    @Test
    public void getById_Should_CallRepository() {
        service.getById(1);

        verify(commentRepository).getById(1);
    }

    @Test
    public void getById_Should_ReturnPost_When_MatchExists() {
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenReturn(comment);

        Comment result = service.getById(1);

        assertEquals(result, comment);
    }

    @Test
    public void getById_Should_ThrowException_When_NoMatchExists() {
        Mockito.when(commentRepository.getById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getById(1));
    }


    @Test
    public void create_Should_CreateComment_When_CalledWithValidParameters() {
        service.create(post, comment, user);

        assertNotNull(comment.getCreation());
        assertNotNull(comment.getLastUpdate());
        assertEquals(user, comment.getCreatedBy());
        assertEquals(post, comment.getPostedOn());
        verify(commentRepository).create(comment);
    }

    @Test
    public void create_Should_ThrowException_When_UserIsBlocked() {
        comment.setCreatedBy(blockedUser);

        assertThrows(UnauthorizedOperationException.class, () -> service.create(post, comment, blockedUser));
        verifyNoInteractions(commentRepository);
    }

    @Test
    public void create_Should_ThrowException_When_UserIsDeleted() {
        comment.setCreatedBy(deletedUser);

        assertThrows(UnauthorizedOperationException.class, () -> service.create(post, comment, deletedUser));
        verifyNoInteractions(commentRepository);
    }

    @Test
    public void update_Should_UpdateComment_When_UserIsCommentCreator() {
        comment.setCreatedBy(user);
        when(service.getById(anyInt())).thenReturn(comment);

        service.update(comment, user);

        verify(commentRepository).update(comment);
    }

    @Test
    public void update_Should_UpdateComment_When_UserIsAdmin() {
        when(service.getById(anyInt())).thenReturn(comment);

        service.update(comment, admin);

        verify(commentRepository).update(comment);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsBlocked() {
        when(service.getById(anyInt())).thenReturn(comment);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(comment, blockedUser));
        verify(commentRepository, never()).update(comment);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsDeleted() {
        when(service.getById(anyInt())).thenReturn(comment);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(comment, deletedUser));
        verify(commentRepository, never()).update(comment);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        when(service.getById(anyInt())).thenReturn(comment);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(comment, diffUser));
        verify(commentRepository, never()).update(comment);
    }

    @Test
    public void delete_Should_DeleteComment_When_UserIsAdmin() {
        when(commentRepository.getById(anyInt())).thenReturn(comment);

        service.delete(comment.getId(), admin);

        verify(commentRepository).delete(comment.getId());
    }

    @Test
    public void delete_Should_DeleteComment_When_UserIsEntityCreator() {
        when(commentRepository.getById(anyInt())).thenReturn(comment);

        service.delete(comment.getId(), user);

        verify(commentRepository).delete(comment.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsBlocked() {
        when(commentRepository.getById(anyInt())).thenReturn(comment);

        assertThrows(UnauthorizedOperationException.class, () -> service.delete(comment.getId(), blockedUser));
        verify(commentRepository, never()).delete(comment.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsDeleted() {
        when(commentRepository.getById(anyInt())).thenReturn(comment);

        assertThrows(UnauthorizedOperationException.class, () -> service.delete(comment.getId(), deletedUser));
        verify(commentRepository, never()).delete(comment.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserNotCreatorOrAdmin() {
        comment.setCreatedBy(user);
        when(commentRepository.getById(anyInt())).thenReturn(comment);

        assertThrows(UnauthorizedOperationException.class, () -> service.delete(comment.getId(), diffUser));
        verify(commentRepository, never()).delete(comment.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_CommentDoesNotExist() {
        when(commentRepository.getById(anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.delete(comment.getId(), user));
        verify(commentRepository, never()).delete(comment.getId());
    }


    @Test
    public void likeComment_Should_CallCreate_When_CommentHasNoReactions() {
        UserCommentReaction expectedReaction = new UserCommentReaction(user.getId(), post.getId(), true);
        service.likeComment(comment, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void likeComment_Should_CallCreate_When_CommentHasNoReactionsByUser() {
        UserCommentReaction expectedReaction = new UserCommentReaction(user.getId(), post.getId(), true);
        comment.setUserCommentReactions(getSetOfCommentReactions());
        service.likeComment(comment, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void likeComment_Should_CallUpdate_When_CommentHasOppositeReactionByUser() {
        Set<UserCommentReaction> reactions = getSetOfCommentReactions();
        reactions.add(new UserCommentReaction(user.getId(), post.getId(), false));
        comment.setUserCommentReactions(reactions);
        UserCommentReaction expectedReaction = new UserCommentReaction(user.getId(), post.getId(), true);

        service.likeComment(comment, user);

        verify(reactionRepository).update(expectedReaction);
    }

    @Test
    public void likeComment_Should_CallDelete_When_CommentHasTheSameReactionByUser() {
        Set<UserCommentReaction> reactions = getSetOfCommentReactions();
        reactions.add(new UserCommentReaction(user.getId(), post.getId(), true));
        comment.setUserCommentReactions(reactions);

        service.likeComment(comment, user);

        verify(reactionRepository).delete(user.getId(), post.getId());
    }

    @Test
    public void dislikeComment_Should_CallCreate_When_CommentHasNoReactions() {
        UserCommentReaction expectedReaction = new UserCommentReaction(user.getId(), post.getId(), false);
        service.dislikeComment(comment, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void dislikeComment_Should_CallCreate_When_CommentHasNoReactionsByUser() {
        UserCommentReaction expectedReaction = new UserCommentReaction(user.getId(), post.getId(), false);
        comment.setUserCommentReactions(getSetOfCommentReactions());
        service.dislikeComment(comment, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void dislikeComment_Should_CallUpdate_When_CommentHasOppositeReactionByUser() {
        Set<UserCommentReaction> reactions = getSetOfCommentReactions();
        reactions.add(new UserCommentReaction(user.getId(), post.getId(), true));
        comment.setUserCommentReactions(reactions);
        UserCommentReaction expectedReaction = new UserCommentReaction(user.getId(), post.getId(), false);

        service.dislikeComment(comment, user);

        verify(reactionRepository).update(expectedReaction);
    }

    @Test
    public void dislikeComment_Should_CallDelete_When_CommentHasTheSameReactionByUser() {
        Set<UserCommentReaction> reactions = getSetOfCommentReactions();
        reactions.add(new UserCommentReaction(user.getId(), post.getId(), false));
        comment.setUserCommentReactions(reactions);

        service.dislikeComment(comment, user);

        verify(reactionRepository).delete(user.getId(), post.getId());
    }

    @Test
    public void checkModifyPermissions_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(comment, blockedUser));
    }

    @Test
    public void checkModifyPermissions_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(comment, deletedUser));
    }

    @Test
    public void checkModifyPermissions_Should_ThrowException_When_UserIsNotCommentCreatorOrAdmin() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(comment, diffUser));
    }

}
