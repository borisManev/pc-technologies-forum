package test.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.models.reactions.UserPostReaction;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.PostRepository;
import com.company.web.forumwebproject.repositories.contracts.UserPostReactionRepository;
import com.company.web.forumwebproject.services.contracts.TagService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Set;

import static com.company.web.forumwebproject.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PostServiceTests {

    @Mock
    PostRepository postRepository;
    @Mock
    TagService tagService;
    @Mock
    UserPostReactionRepository reactionRepository;
    @InjectMocks
    PostServiceImpl service;

    private static Post post;
    private static User user;
    private static User diffUser;
    private static User admin;
    private static User blockedUser;
    private static User deletedUser;
    private static Tag tag;

    @BeforeEach
    public void beforeEach() {
        user = createMockUser();
        blockedUser = createMockBlockedUser();
        deletedUser = createMockDeletedUser();
        diffUser = getDifferentUser();
        admin = createMockAdmin();

        post = createMockPost();
        post.setCreatedBy(user);

        tag = createMockTag();
    }


    @Test
    public void search_Should_CallRepository() {
        service.search(Optional.of("keyword"));

        verify(postRepository).search(Optional.of("keyword"));
    }

    @Test
    public void filter_Should_CallRepository() {
        PostFilterOptions filterOptions = new PostFilterOptions();
        service.filter(filterOptions);

        verify(postRepository).filter(filterOptions);
    }

    @Test
    public void getById_Should_CallRepository() {
        service.getById(1);

        verify(postRepository).getById(1);
    }

    @Test
    public void getById_Should_ReturnPost_When_MatchExists() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        Post result = service.getById(1);

        assertEquals(result, post);
    }

    @Test
    public void getById_Should_ThrowException_When_NoMatchExists() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getById(1));
    }

    @Test
    public void create_Should_CallRepository_When_CalledWithValidParameters() {
        service.create(post, user);

        assertNotNull(post.getCreation());
        assertNotNull(post.getLastUpdate());
        assertEquals(user, post.getCreatedBy());
        verify(postRepository).create(post);
    }

    @Test
    public void create_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.create(post, blockedUser));
    }

    @Test
    public void create_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.create(post, deletedUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        service.update(post, user);

        verify(postRepository).update(post);
    }

    @Test
    public void update_Should_CallRepository_When_UserIsAdmin() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        service.update(post, admin);

        verify(postRepository).update(post);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsBlocked() {
        post.setCreatedBy(blockedUser);
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(post, blockedUser));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsDeleted() {
        post.setCreatedBy(deletedUser);
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(post, deletedUser));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(post, diffUser));
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsCreator() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        service.delete(post.getId(), user);

        verify(postRepository).delete(post.getId());
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsAdmin() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        service.delete(post.getId(), admin);

        verify(postRepository).delete(post.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsBlocked() {
        post.setCreatedBy(blockedUser);
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        assertThrows(UnauthorizedOperationException.class, () -> service.delete(post.getId(), blockedUser));
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsDeleted() {
        post.setCreatedBy(deletedUser);
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        assertThrows(UnauthorizedOperationException.class, () -> service.delete(post.getId(), deletedUser));
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        Mockito.when(postRepository.getById(Mockito.anyInt())).thenReturn(post);

        assertThrows(UnauthorizedOperationException.class, () -> service.delete(post.getId(), diffUser));
    }

    @Test
    public void addTagToPost_Should_PreserveExistingTags() {
        post.setTags(getSetOfTags());

        int numOfTagsBefore = post.getTags().get().size();
        service.addTagToPost(post, tag, user);

        assertEquals(numOfTagsBefore + 1, post.getTags().get().size());
    }

    @Test
    public void addTagToPost_Should_CallRepository_When_UserIsCreator() {
        service.addTagToPost(post, tag, user);

        verify(postRepository).update(post);
    }

    @Test
    public void addTagToPost_Should_CallRepository_When_UserIsAdmin() {
        service.addTagToPost(post, tag, admin);

        verify(postRepository).update(post);
    }

    @Test
    public void addTagToPost_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.addTagToPost(post, tag, blockedUser));
    }

    @Test
    public void addTagToPost_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.addTagToPost(post, tag, deletedUser));
    }

    @Test
    public void addTagToPost_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        assertThrows(UnauthorizedOperationException.class, () -> service.addTagToPost(post, tag, diffUser));
    }

    @Test
    public void addTagToPost_Should_ThrowException_When_PostAlreadyHasTagWithSameName() {
        post.setTags(Set.of(tag));

        assertThrows(EntityDuplicationException.class, () -> service.addTagToPost(post, tag, user));
    }

    @Test
    public void addTagToPost_Should_ReuseTag_When_TagAlreadyExists() {
        Mockito.when(tagService.getByTitle(tag.getTitle())).thenReturn(tag);

        service.addTagToPost(post, tag, user);

        verify(tagService).getByTitle(tag.getTitle());
        verify(tagService, Mockito.times(0)).create(tag);
    }

    @Test
    public void addTagToPost_Should_CreateTag_When_TagDoesNotExist() {
        Mockito.when(tagService.getByTitle(tag.getTitle())).thenThrow(EntityNotFoundException.class);

        service.addTagToPost(post, tag, user);

        verify(tagService).create(tag);
    }

    @Test
    public void removeTagFromPost_Should_CallRepository_When_UserIsPostCreator() {
        Set<Tag> tags = getSetOfTags();
        tags.add(tag);
        post.setTags(tags);

        service.removeTagFromPost(post, tag.getId(), user);

        verify(postRepository).update(post);
    }

    @Test
    public void removeTagFromPost_Should_CallRepository_When_UserIsAdmin() {
        Set<Tag> tags = getSetOfTags();
        tags.add(tag);
        post.setTags(tags);

        service.removeTagFromPost(post, tag.getId(), admin);

        verify(postRepository).update(post);
    }

    @Test
    public void removeTagFromPost_Should_ThrowException_When_TagIdDoesNotExist() {
        post.setTags(getSetOfTags());
        assertThrows(InvalidParameterException.class, () -> service.removeTagFromPost(post, tag.getId(), user));
    }

    @Test
    public void removeTagFromPost_Should_ThrowException_When_TagTitleDoesNotExist() {
        post.setTags(getSetOfTags());
        assertThrows(InvalidParameterException.class, () -> service.removeTagFromPost(post, tag.getTitle(), user));
    }

    @Test
    public void removeTagFromPost_Should_ThrowException_When_PostHasNoTags() {
        assertThrows(InvalidParameterException.class, () -> service.removeTagFromPost(post, tag.getId(), user));
    }

    @Test
    public void removeTagFromPost_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.removeTagFromPost(post, 1, blockedUser));
    }

    @Test
    public void removeTagFromPost_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.removeTagFromPost(post, 1, deletedUser));
    }

    @Test
    public void removeTagFromPost_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        assertThrows(UnauthorizedOperationException.class, () -> service.removeTagFromPost(post, 1, diffUser));
    }

    @Test
    public void likePost_Should_CallCreate_When_PostHasNoReactions() {
        UserPostReaction expectedReaction = new UserPostReaction(user.getId(), post.getId(), true);
        service.likePost(post, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void likePost_Should_CallCreate_When_PostHasNoReactionsByUser() {
        UserPostReaction expectedReaction = new UserPostReaction(user.getId(), post.getId(), true);
        post.setUserPostReactions(getSetOfPostReactions());
        service.likePost(post, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void likePost_Should_CallUpdate_When_PostHasOppositeReactionByUser() {
        Set<UserPostReaction> reactions = getSetOfPostReactions();
        reactions.add(new UserPostReaction(user.getId(), post.getId(), false));
        post.setUserPostReactions(reactions);
        UserPostReaction expectedReaction = new UserPostReaction(user.getId(), post.getId(), true);

        service.likePost(post, user);

        verify(reactionRepository).update(expectedReaction);
    }

    @Test
    public void likePost_Should_CallDelete_When_PostHasTheSameReactionByUser() {
        Set<UserPostReaction> reactions = getSetOfPostReactions();
        reactions.add(new UserPostReaction(user.getId(), post.getId(), true));
        post.setUserPostReactions(reactions);

        service.likePost(post, user);

        verify(reactionRepository).delete(user.getId(), post.getId());
    }

    @Test
    public void dislikePost_Should_CallCreate_When_PostHasNoReactions() {
        UserPostReaction expectedReaction = new UserPostReaction(user.getId(), post.getId(), false);
        service.dislikePost(post, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void dislikePost_Should_CallCreate_When_PostHasNoReactionsByUser() {
        UserPostReaction expectedReaction = new UserPostReaction(user.getId(), post.getId(), false);
        post.setUserPostReactions(getSetOfPostReactions());
        service.dislikePost(post, user);
        verify(reactionRepository).create(expectedReaction);
    }

    @Test
    public void dislikePost_Should_CallUpdate_When_PostHasOppositeReactionByUser() {
        Set<UserPostReaction> reactions = getSetOfPostReactions();
        reactions.add(new UserPostReaction(user.getId(), post.getId(), true));
        post.setUserPostReactions(reactions);
        UserPostReaction expectedReaction = new UserPostReaction(user.getId(), post.getId(), false);

        service.dislikePost(post, user);

        verify(reactionRepository).update(expectedReaction);
    }

    @Test
    public void dislikePost_Should_CallDelete_When_PostHasTheSameReactionByUser() {
        Set<UserPostReaction> reactions = getSetOfPostReactions();
        reactions.add(new UserPostReaction(user.getId(), post.getId(), false));
        post.setUserPostReactions(reactions);

        service.dislikePost(post, user);

        verify(reactionRepository).delete(user.getId(), post.getId());
    }

    @Test
    public void checkModifyPermissions_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(post, blockedUser));
    }

    @Test
    public void checkModifyPermissions_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(post, deletedUser));
    }

    @Test
    public void checkModifyPermissions_Should_ThrowException_When_UserIsNotCommentCreatorOrAdmin() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(post, diffUser));
    }
}
