package test.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.TagRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.web.forumwebproject.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TagServiceTests {

    @Mock
    private TagRepository repository;

    @InjectMocks
    private TagServiceImpl service;

    private User user;
    private User admin;
    private User blockedUser;
    private User deletedUser;
    private Tag tag;

    @BeforeEach
    public void setup() {
        user = createMockUser();
        admin = createMockAdmin();
        blockedUser = createMockBlockedUser();
        deletedUser = createMockDeletedUser();
        tag = createMockTag();
        tag.setId(42);
    }

    @Test
    public void getAll_Should_CallRepository() {
        service.getAll();

        verify(repository).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        service.getById(1);

        verify(repository).getById(1);
    }

    @Test
    public void getById_Should_ReturnPost_When_MatchExists() {
        Mockito.when(repository.getById(Mockito.anyInt())).thenReturn(tag);

        Tag result = service.getById(1);

        assertEquals(result, tag);
    }

    @Test
    public void getById_Should_ThrowException_When_NoMatchExists() {
        Mockito.when(repository.getById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getById(1));
    }

    @Test
    public void getByTitle_Should_CallRepository() {
        service.getByTitle("title");

        verify(repository).getByTitle("title");
    }

    @Test
    public void getByTitle_Should_ReturnPost_When_MatchExists() {
        Mockito.when(repository.getByTitle(anyString())).thenReturn(tag);

        Tag result = service.getByTitle(tag.getTitle());

        assertEquals(result, tag);
    }

    @Test
    public void getByTitle_Should_ThrowException_When_NoMatchExists() {
        Mockito.when(repository.getByTitle(anyString())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getByTitle(tag.getTitle()));
    }

    @Test
    public void create_Should_ThrowException_When_TagWithTitleAlreadyExists() {
        when(repository.getByTitle(tag.getTitle())).thenReturn(tag);

        Tag newTag = new Tag();
        newTag.setTitle(tag.getTitle());

        assertThrows(EntityDuplicationException.class, () -> service.create(newTag));
        verify(repository, never()).create(newTag);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_TagWithTitleDoesNotExist() {
        when(repository.getByTitle(tag.getTitle())).thenThrow(EntityNotFoundException.class);

        service.create(tag);

        verify(repository).create(tag);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.update(tag, blockedUser));
        verify(repository, never()).update(tag);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.update(tag, deletedUser));
        verify(repository, never()).update(tag);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotAdmin() {
        assertThrows(UnauthorizedOperationException.class, () -> service.update(tag, user));
        verify(repository, never()).update(tag);
    }

    @Test
    public void update_Should_ThrowException_When_TagWithTitleAlreadyExists() {
        when(repository.getByTitle(tag.getTitle())).thenReturn(tag);

        Tag updatedTag = new Tag();
        updatedTag.setId(1);
        updatedTag.setTitle(tag.getTitle());

        assertThrows(EntityDuplicationException.class, () -> service.update(updatedTag, admin));
        verify(repository, never()).update(updatedTag);
    }

    @Test
    public void update_Should_CallRepositoryUpdate_When_UserIsAdminAndTagWithTitleDoesNotExist() {
        when(repository.getByTitle(tag.getTitle() + "updated")).thenThrow(EntityNotFoundException.class);

        Tag updatedTag = new Tag();
        updatedTag.setId(tag.getId());
        updatedTag.setTitle(tag.getTitle() + "updated");

        service.update(updatedTag, admin);
        verify(repository).update(updatedTag);
    }

    @Test
    void delete_Should_DeleteTag_When_UserIsAdmin() {
        service.delete(tag.getId(), admin);

        verify(repository).delete(tag.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.delete(tag.getId(), blockedUser));
        verify(repository, never()).delete(tag.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.delete(tag.getId(), deletedUser));
        verify(repository, never()).delete(tag.getId());
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsNotAdmin() {
        assertThrows(UnauthorizedOperationException.class, () -> service.delete(tag.getId(), user));
        verify(repository, never()).delete(tag.getId());
    }


    @Test
    void checkModifyPermissions_Should_ThrowException_When_UserIsBlocked() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(blockedUser));
    }

    @Test
    void checkModifyPermissions_Should_ThrowException_When_UserIsDeleted() {
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(deletedUser));
    }

    @Test
    void checkModifyPermissions_Should_ThrowException_When_UserIsNotAdmin() {
        User user = createMockUser();
        assertThrows(UnauthorizedOperationException.class, () -> service.checkModifyPermissions(user));
    }

    @Test
    void checkModifyPermissions_Should_NotThrowException_When_UserIsAdmin() {
        assertDoesNotThrow(() -> service.checkModifyPermissions(admin));
    }
}