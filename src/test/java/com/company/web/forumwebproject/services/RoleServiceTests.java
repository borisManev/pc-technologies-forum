package test.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.web.forumwebproject.Helpers.createMockDefaultRole;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {
    @Mock
    RoleRepository mockRepository;
    @InjectMocks
    RoleServiceImpl service;

    @Test
    public void create_Should_CallRepository_When_EverythingIsValid() {
        Role mockRole = createMockDefaultRole();
        mockRole.setRoleName("newRole");
        mockRole.setId(10);

        Mockito.when(mockRepository.getByRole(Mockito.anyString())).thenReturn(mockRole);

        service.create(mockRole);
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockRole);

    }

    @Test
    public void delete_Should_CallRepository_When_EverythingIsValid() {
        Role mockRole = createMockDefaultRole();
        service.delete(mockRole.getId());
        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockRole.getId());

    }
}
