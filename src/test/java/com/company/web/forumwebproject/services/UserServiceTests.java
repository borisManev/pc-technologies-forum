package test.java.com.company.web.forumwebproject.services;


import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserFilterOptions;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import com.company.web.forumwebproject.repositories.contracts.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.web.forumwebproject.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {


    @Mock
    UserRepository mockRepository;
    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    UserServiceImpl service;
    RoleServiceImpl roleService;

    private static User mockUser;
    private static User mockDiffUser;
    private static User mockAdmin;
    private static User mockBlockedUser;
    private static User mockDeletedUser;
    private static Role mockRole;

    @BeforeEach
    public void beforeEach() {

        mockBlockedUser = createMockBlockedUser();
        mockDeletedUser = createMockDeletedUser();
        mockDiffUser = getDifferentUser();
        mockAdmin = createMockAdmin();


    }

    @Test
    public void filter_Should_CallRepository() {
        UserFilterOptions userFilterOptions = new UserFilterOptions();
        service.filter(userFilterOptions);

        Mockito.verify(mockRepository, Mockito.times(1)).filter(userFilterOptions);
    }

    @Test
    public void getById_Should_CallRepository() {
        mockUser = createMockUser();
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        service.getById(1);

        Mockito.verify(mockRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        User result = service.getById(1);

        assertEquals(result, mockUser);
    }

    @Test
    public void create_Should_CallRepository_When_EverythingIsValid() {

        mockUser = createMockUser();
        //Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).create(mockUser);
    }


    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        mockUser = createMockUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(mockRepository.getByEmail(Mockito.anyString())).thenReturn(mockUser);

        service.update(mockUser, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void update_Should_Throw_When_UserIsBlocked() {
        mockUser = createMockUser();
        mockUser.setId(1);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(mockUser, mockBlockedUser));
    }

    @Test
    public void update_Should_Throw_When_UserIsDeleted() {
        mockUser = createMockUser();
        mockUser.setId(1);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(mockUser, mockDeletedUser));
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreatorOrAdmin() {
        mockUser = createMockUser();
        mockUser.setId(1);

        assertThrows(UnauthorizedOperationException.class, () -> service.update(mockUser, mockDiffUser));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsCreator() {
        User mockUser = createMockUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        service.delete(1, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void delete_Should_CallRepository_When_UserIsAdmin() {
        User mockUser = createMockUser();
        User mockAdmin = createMockAdmin();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        service.delete(1, mockAdmin);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void delete_Should_Throw_When_UserIsBlocked() {
        User mockUser = createMockUser();
        User mockBlockedUser = createMockBlockedUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getId(), mockBlockedUser));

    }

    @Test
    void delete_Should_Throw_When_UserIsDeleted() {
        User mockUser = createMockUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getId(), mockDeletedUser));

    }

    @Test
    void delete_Should_Throw_When_UserIsNotCreatorOrAdmin() {
        User mockUser = createMockUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getId(), mockDiffUser));

    }

    @Test
    public void block_Should_CallRepository_When_UserIsAdmin() {
        mockUser = createMockUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockUser);
        Mockito.when(roleRepository.getByRole("blocked")).thenReturn(createMockBlockedRole());

        service.blockUser(1, mockAdmin);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void block_Should_Throw_When_UserPerformingActionIsBlocked() {
        mockBlockedUser = createMockBlockedUser();
        mockUser = createMockUser();
        mockBlockedUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockBlockedUser);

        assertThrows(UnauthorizedOperationException.class, () -> service.blockUser(1, mockUser));
    }

    @Test
    public void block_Should_Throw_When_UserIsAlreadyBlocked() {
        mockBlockedUser = createMockBlockedUser();
        mockBlockedUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockBlockedUser);

        assertThrows(UnsupportedOperationException.class, () -> service.blockUser(1, mockAdmin));
    }

    @Test
    public void block_Should_Throw_When_UserPerformingActionIsDeleted() {
        mockBlockedUser = createMockBlockedUser();
        mockUser = createMockUser();
        mockBlockedUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockBlockedUser);

        assertThrows(UnauthorizedOperationException.class, () -> service.blockUser(1, mockDeletedUser));
    }

    @Test
    public void unblock_Should_CallRepository_When_UserIsAdmin() {
        mockBlockedUser = createMockBlockedUser();
        mockBlockedUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockBlockedUser);

        service.unblockUser(1, mockAdmin);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockBlockedUser);
    }

    @Test
    public void unblock_Should_Throw_When_UserPerformingActionIsBlocked() {
        mockBlockedUser = createMockBlockedUser();
        mockUser = createMockUser();
        mockBlockedUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockBlockedUser);

        assertThrows(UnauthorizedOperationException.class, () -> service.unblockUser(1, mockBlockedUser));
    }

    @Test
    public void unblock_Should_Throw_When_UserIsAlreadyUnblocked() {
        mockUser = createMockUser();
        mockUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        assertThrows(UnsupportedOperationException.class, () -> service.unblockUser(1, mockAdmin));
    }

    @Test
    public void unblock_Should_Throw_When_UserPerformingActionIsDeleted() {
        mockBlockedUser = createMockBlockedUser();
        mockUser = createMockUser();
        mockBlockedUser.setId(1);
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockBlockedUser);

        assertThrows(UnauthorizedOperationException.class, () -> service.unblockUser(1, mockDeletedUser));
    }

}
