package test.java.com.company.web.forumwebproject;

import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.reactions.UserCommentReaction;
import com.company.web.forumwebproject.models.reactions.UserPostReaction;
import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.User;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helpers {

    private static final LocalDateTime dateTime = LocalDateTime.of(
            2002, 4, 14, 23, 46, 0);

    public static User createMockUser() {
        User mockUser = new User();
        mockUser.setFirstName("FirstName");
        mockUser.setLastName("LastName");
        mockUser.setUsername("Username");
        mockUser.setPassword("Password1234");
        mockUser.setEmail("FirstName_LastName@email.abc");
        List<Role> mockRoles = new ArrayList<>();
        mockRoles.add(createMockDefaultRole());
        mockUser.setRoles(mockRoles);

        return mockUser;
    }

    public static User createMockAdmin() {
        User mockAdmin = new User();
        mockAdmin.setFirstName("AdminFirstName");
        mockAdmin.setLastName("AdminLastName");
        mockAdmin.setUsername("AdminUsername");
        mockAdmin.setPassword("AdminPassword1234");
        mockAdmin.setEmail("Admin_FirstName_LastName@email.abc");
        List<Role> mockRoles = new ArrayList<>();
        mockRoles.add(createMockAdminRole());
        mockAdmin.setRoles(mockRoles);
        return mockAdmin;
    }

    public static User createMockBlockedUser() {
        User mockUser = createMockUser();
        List<Role> mockRoles = new ArrayList<>();
        mockRoles.add(createMockDefaultRole());
        mockRoles.add(createMockBlockedRole());
        mockUser.setRoles(mockRoles);
        return mockUser;
    }

    public static User createMockDeletedUser() {
        User mockUser = createMockUser();
        List<Role> mockRoles = new ArrayList<>();
        mockRoles.add(createMockDefaultRole());
        mockRoles.add(createMockDeletedRole());
        mockUser.setRoles(mockRoles);
        return mockUser;
    }

    public static Role createMockDefaultRole() {
        Role role = new Role();
        role.setId(1);
        role.setRoleName("general-user");
        return role;
    }

    public static Role createMockAdminRole() {
        Role role = new Role();
        role.setId(2);
        role.setRoleName("admin");
        return role;
    }

    public static Role createMockBlockedRole() {
        Role role = new Role();
        role.setId(3);
        role.setRoleName("blocked");
        return role;
    }

    public static Role createMockDeletedRole() {
        Role role = new Role();
        role.setId(4);
        role.setRoleName("deleted");
        return role;
    }

    public static PhoneNumber createMockPhoneNumber() {
        PhoneNumber mockPhoneNumber = new PhoneNumber();
        mockPhoneNumber.setId(createMockUser().getId());
        mockPhoneNumber.setPhoneNumber("+359888123123");
        return mockPhoneNumber;
    }

    public static Tag createMockTag() {
        Tag mockTag = new Tag();
        mockTag.setTitle("mock-tag");
        mockTag.setDescription("This is a mock tag.");
        return mockTag;
    }

    public static Comment createMockComment() {
        Comment mockComment = new Comment();
        mockComment.setContent("This is a mock comment.");
        return mockComment;
    }

    public static UserCommentReaction createMockCommentReaction() {
        UserCommentReaction reaction = new UserCommentReaction();
        reaction.setLiked(true);
        reaction.setCommentId(1);
        reaction.setUserId(1);
        return reaction;
    }

    public static UserPostReaction createMockPostReaction() {
        UserPostReaction reaction = new UserPostReaction();
        reaction.setLiked(true);
        reaction.setPostId(1);
        reaction.setUserId(1);
        return reaction;
    }

    public static Post createMockPost() {
        Post mockPost = new Post();
        mockPost.setTitle("Title");
        mockPost.setContent("This is a mock post.");
        return mockPost;
    }

    public static User getDifferentUser() {
        User mockUser = createMockUser();
        mockUser.setId(66);
        mockUser.setUsername("differentUsername");
        mockUser.setEmail("differentEmail@email.abc");
        return mockUser;
    }

    public static Set<Tag> getSetOfTags() {
        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag(1, "tag1", ""));
        tags.add(new Tag(2, "tag2", ""));
        tags.add(new Tag(3, "tag3", ""));
        tags.add(new Tag(4, "tag4", ""));
        return tags;
    }

    public static Set<UserPostReaction> getSetOfPostReactions() {
        Set<UserPostReaction> reactions = new HashSet<>();
        reactions.add(new UserPostReaction(0, 3, true));
        reactions.add(new UserPostReaction(0, 4, false));
        reactions.add(new UserPostReaction(0, 5, true));
        reactions.add(new UserPostReaction(0, 6, true));
        return reactions;
    }

    public static Set<UserCommentReaction> getSetOfCommentReactions() {
        Set<UserCommentReaction> reactions = new HashSet<>();
        reactions.add(new UserCommentReaction(0, 3, true));
        reactions.add(new UserCommentReaction(0, 4, false));
        reactions.add(new UserCommentReaction(0, 5, true));
        reactions.add(new UserCommentReaction(0, 6, true));
        return reactions;
    }
}