package main.java.com.company.web.forumwebproject.controllers.rest;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.PhoneMapper;
import com.company.web.forumwebproject.helpers.UserMapper;
import com.company.web.forumwebproject.models.comment.CommentDtoOut;
import com.company.web.forumwebproject.models.post.PostDtoOut;
import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.models.user.*;
import com.company.web.forumwebproject.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final CommentController commentController;
    private final PostController postController;
    private final UserService service;
    private final UserMapper userMapper;
    private final PhoneMapper phoneMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(CommentController commentController, PostController postController, UserService service, UserMapper userMapper, PhoneMapper phoneMapper, AuthenticationHelper authenticationHelper) {
        this.commentController = commentController;
        this.postController = postController;
        this.service = service;
        this.userMapper = userMapper;
        this.phoneMapper = phoneMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<UserDtoOut> search(@RequestParam Optional<String> search) {
        try {
            return service.search(search).stream()
                    .map(userMapper::ObjectToDto)
                    .collect(Collectors.toList());
        } catch (InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

//    @GetMapping
//    public List<UserDtoOut> getUsers(@RequestParam(required = false) String username, @RequestParam(required = false) String firstName,
//                                     @RequestParam(required = false) String lastName, @RequestParam(required = false) String phoneNumber,
//                                     @RequestParam(required = false)String email, @RequestParam(required = false) String sort
//    ) {
//        try {
//            UserFilterOptions userFilterOptions = new UserFilterOptions(username, firstName, lastName, phoneNumber, email, sort)
//
//            return service.filter(userFilterOptions)
//                    .stream()
//                    .map(userMapper::ObjectToDto)
//                    .collect(Collectors.toList());
//        } catch (InvalidParameterException | UnsupportedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        }
//    }

    @GetMapping("/{id}")
    public UserDtoOut getUser(@PathVariable int id) {
        return userMapper.ObjectToDto(getUserById(id));
    }

    private User getUserById(int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<UserDtoOut> filter(@RequestParam(required = false) String username, @RequestParam(required = false) String firstName,
                                   @RequestParam(required = false) String lastName, @RequestParam(required = false) String phoneNumber,
                                   @RequestParam(required = false) String email, @RequestParam(required = false) String sort,
                                   @RequestParam(required = false) String lastLogin, @RequestParam(required = false) String creation,
                                   HttpHeaders headers
    ) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(headers);
            UserFilterOptions userFilterOptions = new UserFilterOptions(username, firstName, lastName, phoneNumber, email, phoneNumber, sort, false, false, false);
            List<User> users = service.filter(userFilterOptions);
            return users == null ? null : users.stream()
                    .map(userMapper::ObjectToDto)
                    .collect(Collectors.toList());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        //  return userMapper.ObjectToDTO(service.filter(user,username, email, firstName));
    }


    @PostMapping
    public UserDtoOut createUser(@RequestHeader HttpHeaders headers, @Valid @RequestBody UserDtoIn userDto) {
        try {
            User user = userMapper.dtoToObject(userDto);
            service.create(user);
            return userMapper.ObjectToDto(user);
        } catch (EntityDuplicationException | UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    //  TODO // updatePhone?
    @PutMapping("/{id}")
    public UserDtoOut update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDtoIn userDTOIn) {
        try {
            User user1 = authenticationHelper.tryGetUser(headers);
            User updatedUser = userMapper.dtoToObject(userDTOIn);
            updatedUser.setId(id);
            service.update(updatedUser, user1);
            updatedUser = getUserById(id);
            return userMapper.ObjectToDto(updatedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicationException | UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    //TODO
    @DeleteMapping("/{id}")
    public UserDtoOut delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User deleteUser = getUserById(id);
            service.delete(id, user);
            return userMapper.ObjectToDto(deleteUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    //--------------------------------ROLES-------------------------------------

    @PutMapping("/{id}/block")
    public UserDtoOut blockUser(@RequestHeader HttpHeaders headers,
                                @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.blockUser(id, user);
            User userToBeBlocked = getUserById(id);
            return userMapper.ObjectToDto(userToBeBlocked);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/unblock")
    public UserDtoOut unblockUser(@RequestHeader HttpHeaders headers,
                                  @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.unblockUser(id, user);
            User userToBeUnblocked = getUserById(id);
            return userMapper.ObjectToDto(userToBeUnblocked);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/adminUser")
    public UserDtoOut makeUserAdmin(@RequestHeader HttpHeaders headers,
                                    @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.makeUserAdmin(id, user);
            User userToBeMadeAdmin = getUserById(id);
            return userMapper.ObjectToDto(userToBeMadeAdmin);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}/roles")
    public List<Role> getRoles(@PathVariable int id) {
        return getUserById(id).getRoles();
    }

    @GetMapping("/{userId}/roles/{roleId}")
    public Role getRoles(@PathVariable int userId, @PathVariable int roleId) {
        User user = getUserById(userId);
        if (user.getRoles().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with id %d has no roles.", userId));
        } else {
            return user.getRoles().stream()
                    .filter(r -> r.getId() == roleId)
                    .findFirst()
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                            String.format("User with id %d has no roles with id %d.", userId, roleId)));
        }
    }

    @GetMapping("/{userId}/roles/{roleName}")
    public Role getRoles(@PathVariable int userId, @PathVariable String roleName) {
        User user = getUserById(userId);
        if (user.getRoles().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with id %d has no roles.", userId));
        } else {
            return user.getRoles().stream()
                    .filter(r -> r.getRoleName().equals(roleName))
                    .findFirst()
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                            String.format("User with id %d has no roles named %s.", userId, roleName)));
        }
    }

    //--------------------------------COMMENTS-------------------------------------


    @GetMapping("/{id}/comments")
    public List<CommentDtoOut> filterComments(@PathVariable int id,
                                              @RequestParam(required = false) String content,
                                              @RequestParam(required = false) String sort) {
        //verify that a user with given id exists
        User user = getUserById(id);
        return commentController.filter(content, user.getUsername(), null, sort);
    }

    //--------------------------------POSTS-------------------------------------
    @GetMapping("/{id}/posts")
    public List<PostDtoOut> filterPosts(@PathVariable int id,
                                        @RequestParam(required = false) String title,
                                        @RequestParam(required = false) String content,
                                        @RequestParam(required = false) String titleAndContent,
                                        @RequestParam(required = false, name = "tag") String tagTitle,
                                        @RequestParam(required = false) String sort,
                                        @RequestParam(required = false) Integer limit) {
        //verify that a user with given id exists
        User user = getUserById(id);
        return postController.filter(title, content, titleAndContent, user.getUsername(), tagTitle, sort, limit);
    }
    //--------------------------------PhoneNumber-------------------------------------
    @GetMapping("/{id}/phoneNumber")
    public String getNumber(@PathVariable int id) {
        return String.valueOf(getUserById(id).getPhoneNumber().orElse(null));
    }

    @PostMapping("/{id}/phoneNumber")
    public UserDtoOut createTag(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                @Valid @RequestBody PhoneNumberDtoIn phoneNumberDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            //PhoneNumber phoneNumber = getNumber(id);
            PhoneNumber phoneNumber = phoneMapper.dtoToObject(phoneNumberDtoIn);
            service.addPhoneToUser(user, phoneNumber);
            return userMapper.ObjectToDto(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicationException | InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{userId}/phoneNumber")
    public UserDtoOut deleteNumber(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            service.removePhoneFromUser(user);
            return userMapper.ObjectToDto(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}


//    @GetMapping("/{userId}/phones")
//    public Optional<Phone> getPhones(@PathVariable int userId) {
//        return getUserById(userId).getPhoneNumber();
//    }

//    @GetMapping("/{userId}/phones/{phonesId}")
//    public Phone getPhones(@PathVariable int userId, @PathVariable int phonesId) {
//        User user = getUserById(userId);
//        if (user.getPhoneNumber().isEmpty()) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
//                    String.format("User with id %d has no phone number.", userId));
//        } else {
//            return user.getPhoneNumber().stream()
//                    .filter(r -> r.getId() == phonesId)
//                    .findFirst()
//                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
//                            String.format("User with id %d has no phone number with id %d.", userId, phonesId)));
//        }
//    }

//    private User createUser(User user) {
//        try {
//            service.create(user);
//            return user;
//        } catch (EntityDuplicationException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }

// TODO

//    @PostMapping("/{id}/roles")
//    public UserDtoOutput createRole(@RequestHeader HttpHeaders headers, @PathVariable int id,
//                                @Valid @RequestBody RoleDtoInput roleDto) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            User userTwo = getUserById(id);
//            Role  role  = roleMapper.dtoToObject(id, roleDto);
//            try {
//                role = roleService.getByRole(role.getRole());
//            } catch (EntityNotFoundException e) {
//                role = roleService.create(role);
//            }
//
//            if (role.getRole().isEmpty()) {
//                role.setRole(new String());
//            } else {
//                Role finalRole = role;
//                if (userTwo.getRoles().stream().anyMatch(r -> r.getRole().equals(finalRole.getRole()))) {
//                    throw new ResponseStatusException(HttpStatus.CONFLICT,
//                            String.format("User with id %d already has role with title %s.", id, role.getRole()));
//                }
//            }
//            userTwo.getRoles().add(role);
//            return userMapper.ObjectToDTO(updateUser(role, user));
//        } catch (DuplicateEntityException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }
//    private User updateUser(Role role, User user) {
//        try {
//            return service.update(role, user);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }
