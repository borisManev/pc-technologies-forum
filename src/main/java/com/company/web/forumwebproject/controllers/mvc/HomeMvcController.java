package main.java.com.company.web.forumwebproject.controllers.mvc;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.PostService;
import com.company.web.forumwebproject.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.userIsAdmin;
import static com.company.web.forumwebproject.utils.FormattingUtils.dateTimeFormatter;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final PostService postService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(PostService postService, UserService userService, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            return userIsAdmin(user);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return false;
        }
    }

    //TODO method is kinda ugly should fix later
    @GetMapping
    public String showHomePage(@RequestParam(required = false) boolean sortMostCommented, Model model) {
        PostFilterOptions filterOptions = new PostFilterOptions();
        if (sortMostCommented) {
            filterOptions.setSort("commented_desc");
        } else {
            filterOptions.setSort("creation_desc");
        }
        filterOptions.setLimit(10);

        model.addAttribute("numPosts", postService.getNumOfPosts());
        model.addAttribute("numUsers", userService.getNumberOfUsers());


        model.addAttribute("posts_latest", !sortMostCommented);
        model.addAttribute("posts_most_commented", sortMostCommented);
        model.addAttribute("posts", postService.filter(filterOptions));
        model.addAttribute("dateTimeFormatter", dateTimeFormatter);
        return "index";
    }

    @GetMapping("/about")
    public String showAboutMeInformation() {
        return "about";
    }

//    @GetMapping("/contact")
//    public String showContactMeInformation() {
//        return "contact";
//    }
}
