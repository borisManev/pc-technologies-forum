package main.java.com.company.web.forumwebproject.controllers.rest;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.CommentMapper;
import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentDtoIn;
import com.company.web.forumwebproject.models.comment.CommentDtoOut;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/comments")
@Tag(name = "Published comments", description = "Published comments related resources")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CommentController(CommentService commentService, CommentMapper commentMapper,
                             AuthenticationHelper authenticationHelper) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    @Operation(summary = "Get all published comments", description = "Get all of the published comments that exist " +
            "on all posts")
    @ApiResponse(responseCode = "200",
            description = "Retrieve zero or more comments",
            content = @Content(mediaType = "json"))
    public List<CommentDtoOut> search(@Parameter(description = "Search keyword", example = "monitor")
                                      @RequestParam Optional<String> search) {
        return commentService.search(search).stream()
                .map(commentMapper::ObjectToDto)
                .collect(Collectors.toList());

    }

    @GetMapping("/filter")
    public List<CommentDtoOut> filter(@RequestParam(required = false) String content,
                                      @RequestParam(required = false) String username,
                                      @RequestParam(required = false) Integer postId,
                                      @RequestParam(required = false) String sort) {

        try {
            CommentFilterOptions filterOptions = new CommentFilterOptions(content, username, postId, sort);
            return commentService.filter(filterOptions)
                    .stream()
                    .map(commentMapper::ObjectToDto)
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CommentDtoOut get(@PathVariable int id) {
        return commentMapper.ObjectToDto(getById(id));
    }

    private Comment getById(int id) {
        try {
            return commentService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public CommentDtoOut update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                @Valid @RequestBody CommentDtoIn commentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(commentDto);
            comment.setId(id);
            commentService.update(comment, user);
            comment = getById(id);
            return commentMapper.ObjectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public CommentDtoOut delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentService.getById(id);
            commentService.delete(id, user);
            return commentMapper.ObjectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/like")
    public CommentDtoOut like(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = getById(id);
            commentService.likeComment(comment, user);
            return commentMapper.ObjectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/dislike")
    public CommentDtoOut dislike(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = getById(id);
            commentService.dislikeComment(comment, user);
            return commentMapper.ObjectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
