package main.java.com.company.web.forumwebproject.controllers.mvc;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.*;
import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentDtoIn;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.models.comment.CommentFilterOptionsDto;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostDtoIn;
import com.company.web.forumwebproject.models.post.PostFilterOptionsDto;
import com.company.web.forumwebproject.models.reactions.UserCommentReaction;
import com.company.web.forumwebproject.models.reactions.UserPostReaction;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.tag.TagDtoIn;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.CommentService;
import com.company.web.forumwebproject.services.contracts.PostService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.userIsAdmin;
import static com.company.web.forumwebproject.utils.FormattingUtils.dateTimeFormatter;

@Controller
@RequestMapping("/posts")
public class PostMvcController {
    private final PostService postService;
    private final PostMapper postMapper;
    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authHelper;
    private final PostFilterMapper postFilterMapper;
    private final CommentFilterMapper commentFilterMapper;
    private final TagMapper tagMapper;

    @Autowired
    public PostMvcController(PostService postService, PostMapper postMapper, CommentService commentService,
                             CommentMapper commentMapper, AuthenticationHelper authHelper,
                             PostFilterMapper postFilterMapper, CommentFilterMapper commentFilterMapper,
                             TagMapper tagMapper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authHelper = authHelper;
        this.postFilterMapper = postFilterMapper;
        this.commentFilterMapper = commentFilterMapper;
        this.tagMapper = tagMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currUser")
    public String populateCurrUser(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        try {
            User user = authHelper.tryGetCurrentUser(session);
            return userIsAdmin(user);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return false;
        }
    }

    @ModelAttribute("dateTimeFormatter")
    public DateTimeFormatter populateDateTimeFormatter() {
        return dateTimeFormatter;
    }


    @GetMapping
    public String showAllPosts(Model model,
                               @ModelAttribute("postFilterOptions") PostFilterOptionsDto postFilterOptionsDto) {
        if (!(boolean) model.getAttribute("isAuthenticated")) {
            return "notFound";
        }
        List<Post> posts = postService.filter(postFilterMapper.dtoToObject(postFilterOptionsDto));
        model.addAttribute("posts", posts);
        return "posts";
    }

    @GetMapping("/{postId}")
    public String showPost(@PathVariable int postId, Model model, HttpSession session,
                           @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);
        return "post";
    }

    @PostMapping("/{postId}/comments")
    public String createComment(@PathVariable int postId,
                                Model model,
                                @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                @Valid @ModelAttribute("oldCommentDto") CommentDtoIn commentDto,
                                BindingResult bindingResult,
                                HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);
        Post post = (Post) model.getAttribute("post");

        if (bindingResult.hasErrors()) {
            return "post";
        }
        try {
            User user = authHelper.tryGetCurrentUser(session);
            Comment comment = commentMapper.dtoToObject(commentDto);
            commentService.create(post, comment, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("content", "auth_error", e.getMessage());
            return "post";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/delete")
    public String deleteComment(@PathVariable int postId,
                                @PathVariable int commentId,
                                @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                Model model,
                                HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);

        try {
            User user = authHelper.tryGetCurrentUser(session);
            commentService.delete(commentId, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "post";
        }
    }

    @GetMapping("/create")
    public String showCreatePostPage(Model model) {
        model.addAttribute("postDto", new PostDtoIn());
        return "createPost";
    }

    @PostMapping("/create")
    public String createPost(@Valid @ModelAttribute("postDto") PostDtoIn postDto,
                             BindingResult bindingResult,
                             HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "createPost";
        }
        try {
            User user = authHelper.tryGetCurrentUser(session);
            Post newPost = postMapper.dtoToObject(postDto);
            Post post = postService.create(newPost, user);
            return "redirect:/posts/" + post.getId();
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("content", "auth_error", e.getMessage());
            return "createPost";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/update")
    public String showUpdateCommentPage(@PathVariable int postId,
                                        @PathVariable int commentId,
                                        @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                        Model model,
                                        HttpSession session) {

        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);
        Comment commentToEdit;
        try {
            commentToEdit = commentService.getById(commentId);
        } catch (EntityNotFoundException e) {
            return "notFound";
        }
        model.addAttribute("commentDtoEdit", new CommentDtoIn(commentToEdit.getContent()));
        model.addAttribute("commentToEditId", commentId);
        return "post";
    }

    @PostMapping("/{postId}/comments/{commentId}/update")
    public String updateComment(@PathVariable int postId,
                                @PathVariable int commentId,
                                @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                Model model,
                                @Valid @ModelAttribute("commentDtoEdit") CommentDtoIn commentDto,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);
        model.addAttribute("commentToEditId", commentId);

        if (bindingResult.hasErrors()) {
            return "post";
        }

        try {
            User user = authHelper.tryGetCurrentUser(session);
            Comment comment = commentMapper.dtoToObject(commentDto);
            comment.setId(commentId);
            commentService.update(comment, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            String message = e.getMessage().replace("entity", "comment");
            bindingResult.rejectValue("content", "auth_error", message);
            return "post";
        } catch (EntityNotFoundException e) {
            return "notFound";
        }
    }


    @GetMapping("/{postId}/delete")
    public String deletePost(@PathVariable int postId,
                             @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                             Model model,
                             HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";

        try {
            User user = authHelper.tryGetCurrentUser(session);
            postService.delete(postId, user);
            return "redirect:/posts";
        } catch (UnauthorizedOperationException e) {
            String message = e.getMessage().replace("entity", "post");
            model.addAttribute("auth_error", message);
            return "post";
        }
    }

    @GetMapping("/{postId}/update")
    public String showUpdatePostPage(@PathVariable int postId,
                                     @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                     Model model) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        Post post = (Post) model.getAttribute("post");
        assert post != null;
        model.addAttribute("postDtoEdit", new PostDtoIn(post.getTitle(), post.getContent()));
        return "post";
    }


    @PostMapping("/{postId}/update")
    public String updatePost(@PathVariable int postId,
                             @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                             Model model,
                             @Valid @ModelAttribute("postDtoEdit") PostDtoIn postDto,
                             BindingResult bindingResult,
                             HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";

        if (bindingResult.hasErrors()) {
            return "post";
        }

        try {
            User user = authHelper.tryGetCurrentUser(session);
            Post post = postMapper.dtoToObject(postDto);
            post.setId(postId);
            postService.update(post, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            String message = e.getMessage().replace("entity", "post");
            bindingResult.rejectValue("content", "auth_error", message);
            return "post";
        } catch (EntityNotFoundException e) {
            return "notFound";
        }
    }

    @GetMapping("/{postId}/tags")
    public String showUpdateTagPage(@PathVariable int postId,
                                    @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                    Model model) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        Post post = (Post) model.getAttribute("post");
        assert post != null;
        model.addAttribute("tagDto", new TagDtoIn());
        return "post";
    }

    @PostMapping("/{postId}/tags")
    public String createTagOnPost(@PathVariable int postId,
                                  @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                  Model model,
                                  @Valid @ModelAttribute("tagDto") TagDtoIn tagDto,
                                  BindingResult bindingResult,
                                  HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);

        if (bindingResult.hasErrors()) {
            return "post";
        }

        try {
            User user = authHelper.tryGetCurrentUser(session);
            Post post = (Post) model.getAttribute("post");
            Tag tag = tagMapper.dtoToObject(tagDto);
            postService.addTagToPost(post, tag, user);
            return "redirect:/posts/" + postId + "/tags";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("title", "auth_error", e.getMessage());
            return "post";
        } catch (InvalidParameterException e) {
            bindingResult.rejectValue("title", "format_error", e.getMessage());
            return "post";
        } catch (EntityDuplicationException e) {
            bindingResult.rejectValue("title", "duplication_error", e.getMessage());
            return "post";
        } catch (EntityNotFoundException e) {
            return "notFound";
        }
    }

    @GetMapping("/{postId}/tags/{tagId}/remove")
    public String deleteTag(@PathVariable int postId,
                            @PathVariable int tagId,
                            @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                            Model model,
                            HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        loadReactions(postId, model, session);

        try {
            User user = authHelper.tryGetCurrentUser(session);
            Post post = (Post) model.getAttribute("post");
            postService.removeTagFromPost(post, tagId, user);
            return "redirect:/posts/" + postId + "/tags";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "post";
        }
    }

    @GetMapping("/{postId}/like")
    public String likePost(@PathVariable int postId,
                           @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                           Model model,
                           HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        try {
            User user = authHelper.tryGetCurrentUser(session);
            Post post = (Post) model.getAttribute("post");

            loadReactions(postId, model, session);

            postService.likePost(post, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "post";
        }
    }

    private UserPostReaction getPostReactionByUser(HttpSession session, int postId) {
        Post post;
        User user;
        try {
            post = postService.getById(postId);
            user = authHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return null;
        }
        if (post.getUserPostReactions().isPresent()) {
            return post.getUserPostReactions().get().stream()
                    .filter(r -> r.getUserId() == user.getId() && r.getPostId() == post.getId())
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    @GetMapping("/{postId}/dislike")
    public String dislikePost(@PathVariable int postId,
                              @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                              Model model,
                              HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        try {
            User user = authHelper.tryGetCurrentUser(session);
            Post post = (Post) model.getAttribute("post");

            loadReactions(postId, model, session);

            postService.dislikePost(post, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "post";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/like")
    public String likeComment(@PathVariable int postId,
                              @PathVariable int commentId,
                              @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                              Model model,
                              HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        try {
            User user = authHelper.tryGetCurrentUser(session);
            Comment comment = commentService.getById(commentId);

            loadReactions(postId, model, session);

            commentService.likeComment(comment, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "post";
        }
    }

    @GetMapping("/{postId}/comments/{commentId}/dislike")
    public String dislikeComment(@PathVariable int postId,
                                 @PathVariable int commentId,
                                 @ModelAttribute("commentFilterOptions") CommentFilterOptionsDto commentFilterOptionsDto,
                                 Model model,
                                 HttpSession session) {
        if (!loadPostFromId(postId, model, commentFilterOptionsDto)) return "notFound";
        try {
            User user = authHelper.tryGetCurrentUser(session);
            Comment comment = commentService.getById(commentId);

            loadReactions(postId, model, session);

            commentService.dislikeComment(comment, user);
            return "redirect:/posts/" + postId;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "post";
        }
    }

    private void loadReactions(int postId, Model model, HttpSession session) {
        model.addAttribute("postIsLiked", getPostReactionByUser(session, postId));
        model.addAttribute("commentReactionMap", getCommentReactions(postId, session));
    }

    //returns a map that contains the comment id as key and an integer:{1;0;-1) for the reaction of the current
    //user in the session on the comments for the post with given id
    //0 - no reaction by user
    //1 - like by user
    //-1 - dislike by user
    private HashMap<Integer, Integer> getCommentReactions(int postId, HttpSession session) {
        HashMap<Integer, Integer> map = new HashMap<>();
        try {
            Post post = postService.getById(postId);
            User user = authHelper.tryGetCurrentUser(session);
            if (post.getComments().isEmpty()) return null;
            for (Comment comment : post.getComments().get()) {
                if (comment.getUserCommentReactions().isPresent()) {
                    UserCommentReaction userCommentReaction = comment.getUserCommentReactions().get()
                            .stream()
                            .filter(r -> r.getUserId() == user.getId())
                            .findFirst().orElse(null);
                    if (userCommentReaction != null) {
                        map.put(comment.getId(), userCommentReaction.isLiked() ? 1 : -1);
                    } else {
                        map.put(comment.getId(), 0);
                    }
                } else {
                    map.put(comment.getId(), 0);
                }
            }
            return map;
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            return null;
        }
    }

    private boolean loadPostFromId(@PathVariable int postId, Model model, CommentFilterOptionsDto filterOptionsDto) {
        Post post;
        try {
            post = postService.getById(postId);
        } catch (EntityNotFoundException e) {
            return false;
        }
        CommentFilterOptions filterOptions = commentFilterMapper.dtoToObject(filterOptionsDto);
        filterOptions.setPostId(postId);

        model.addAttribute("comments", commentService.filter(filterOptions));
        model.addAttribute("newCommentDto", new CommentDtoIn());
        model.addAttribute("post", post);
        return true;
    }
}
