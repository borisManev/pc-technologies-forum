package main.java.com.company.web.forumwebproject.controllers.mvc;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.PhoneMapper;
import com.company.web.forumwebproject.helpers.UserFilterMapper;
import com.company.web.forumwebproject.helpers.UserMapper;
import com.company.web.forumwebproject.models.authModels.ProfileUpdateDto;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserFilterOptions;
import com.company.web.forumwebproject.models.user.UsersFilterDto;
import com.company.web.forumwebproject.services.contracts.*;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.*;
import static com.company.web.forumwebproject.utils.FormattingUtils.dateTimeFormatter;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final RoleService roleService;
    private final CommentService commentService;
    private final PostService postService;
    private final UserMapper userMapper;
    private final PhoneService phoneService;
    private final AuthenticationHelper authenticationHelper;
    private final UserFilterMapper userFilterMapper;
    private final PhoneMapper phoneMapper;
    public static final String UNAUTHORISED_NOT_LOGGED = "You have to be logged in order to access the requested resource!";

    @Autowired
    public UserMvcController(UserService userService, RoleService roleService, CommentService commentService,
                             PostService postService, UserMapper userMapper, PhoneService phoneService, AuthenticationHelper authenticationHelper,
                             UserFilterMapper userFilterMapper, PhoneMapper phoneMapper) {
        this.userService = userService;
        this.roleService = roleService;
        this.commentService = commentService;
        this.postService = postService;
        this.userMapper = userMapper;
        this.phoneService = phoneService;
        this.authenticationHelper = authenticationHelper;
        this.userFilterMapper = userFilterMapper;
        this.phoneMapper = phoneMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currUserUsername")
    public String populateCurrUsername(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            return (String) session.getAttribute("currentUser");
        }
        return "";
    }

    @ModelAttribute("isCurrUserBlocked")
    public boolean populateIsBlocked(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            return userIsBlocked(user);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return false;
        }
    }
    @ModelAttribute("isAdmin")
    public boolean populateIsCurrAdmin(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            return userIsAdmin(user);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return false;
        }
    }
//
//    @ModelAttribute("getAdminRoles")
//    public Map<User, List<Boolean>> areAdmins() {
//        List<User> users = userService.getAll();
//        Map<User, List<Boolean>> areAdmins = new HashMap<>();
//        for (User user : users) {
//            boolean isAdmin = userIsAdmin(userService.getById(user.getId()));
//            areAdmins.put(user, Collections.singletonList(isAdmin));
//        }
//        return areAdmins;
//    }
//
//    @ModelAttribute("getBlockedRoles")
//    public Map<User, List<Boolean>> areBlocked() {
//        List<User> users = userService.getAll();
//        Map<User, List<Boolean>> areBlocked = new HashMap<>();
//        for (User user : users) {
//            boolean isBlocked = userIsBlocked(userService.getById(user.getId()));
//            areBlocked.put(user, Collections.singletonList(isBlocked));
//        }
//        return areBlocked;
//    }
//
//    @ModelAttribute("getDeletedRoles")
//    public Map<User, List<Boolean>> areDeleted() {
//        List<User> users = userService.getAll();
//        Map<User, List<Boolean>> areDeleted = new HashMap<>();
//        for (User user : users) {
//            boolean isDeleted = userIsDeleted(userService.getById(user.getId()));
//            areDeleted.put(user, Collections.singletonList(isDeleted));
//        }
//        return areDeleted;
//    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            return userIsAdmin(user);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return false;
        }
    }


    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/";
        }
        try {
            user = userService.getById(id);
            model.addAttribute("isBlocked", userIsBlocked(userService.getById(id)));
            model.addAttribute("isDeleted", userIsDeleted(userService.getById(id)));
            model.addAttribute("isThisUserAdmin", userIsAdmin(userService.getById(id)));
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "accessDenied";
        }


        model.addAttribute("user", user);
        model.addAttribute("dateTimeFormatter", dateTimeFormatter);
        model.addAttribute("currentUser", currentUser);

        model.addAttribute("currentUserRole", user.getRoles().
                stream().
                anyMatch(r -> r.
                        getRoleName().
                        equalsIgnoreCase("admin")));
        return "user";
    }

    @GetMapping("/profile")
    public String showMyProfile(HttpSession session, Model model) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/";
        }

        try {
            model.addAttribute("user", currentUser);
            return "redirect:/users/" + currentUser.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "notFound";
        }
    }

    @GetMapping()
    public String showUsersPage(@ModelAttribute("userFilterOptions") UsersFilterDto usersFilterDto,
                                HttpSession session,
                                Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("errorMessage", UNAUTHORISED_NOT_LOGGED);
            return "notFound";
        }

        UserFilterOptions userFilterOptions = userFilterMapper.dtoToObject(usersFilterDto);
        model.addAttribute("users", userService.filter(userFilterOptions));
        model.addAttribute("userFilterOptions", usersFilterDto);

        return "users";
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            ProfileUpdateDto userDto = userMapper.userToDtoUpdate(user);

            model.addAttribute("userId", id);
            model.addAttribute("user", user);
            model.addAttribute("userDto", userDto);
            return "userUpdate";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("userDto") ProfileUpdateDto dto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User currUser;
        try {
            currUser = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("user", currUser);
        } catch (UnsupportedOperationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "userUpdate";
        }

        try {
            User user = userMapper.fromDto(dto, id);
            if (dto.getOldPassword().equals(dto.getOldPasswordConfirm())) {
                authenticationHelper.verifyAuthentication(user.getUsername(), dto.getOldPassword());
            } else {
                bindingResult.rejectValue("oldPasswordConfirm", "wrong_confirmation",
                        "Passwords do not match.");
                return "userUpdate";
            }

            userService.update(user, currUser);
            if (user.getPhoneNumber().isPresent()) {
                phoneService.update(user.getPhoneNumber().orElse(null), currUser);
            }
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        } catch (EntityDuplicationException e) {
            bindingResult.rejectValue("phoneNumber", "duplicate_user", e.getMessage());
            return "userUpdate";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("oldPassword", "invalid_password", "Invalid authentication.");
            return "userUpdate";
//            model.addAttribute("error", "Password is invalid.");
//            return "accessDenied";
        }
    }


    @PostMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, user);
            model.addAttribute("isDelete", userIsDeleted(userService.getById(id)));
            model.addAttribute("role", user.getRoles().stream().anyMatch(r -> r.getRoleName().equalsIgnoreCase("admin")));
//
            return "redirect:/users/"+id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "accessDenied";
        }

    }

    @PostMapping("/{id}/block")
    public String blockUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.blockUser(id, user);
            model.addAttribute("role", user.getRoles()
                    .stream()
                    .anyMatch(r -> r.getRoleName().equalsIgnoreCase("admin")));
            model.addAttribute("isBlocked", userIsBlocked(userService.getById(id)));
            model.addAttribute("isBlocked", userIsBlocked(userService.getById(id)));
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "accessDenied";
        }


    }

    @PostMapping("/{id}/unblock")
    public String unblockUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.unblockUser(id, user);
            model.addAttribute("role", user.getRoles().stream().anyMatch(r -> r.getRoleName().equalsIgnoreCase("admin")));
            model.addAttribute("isBlocked", userIsBlocked(userService.getById(id)));

            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "accessDenied";
        }

    }

    //    @ModelAttribute("userDetails")
//    public Map<String, Object> getUserDetails(@RequestParam("id") int id){
//        Map<String, Object> userDetails = new HashMap<>();
//        userDetails.put("isAdmin", userIsAdmin(userService.getById(id)));
//        userDetails.put("isBlocked", userIsBlocked(userService.getById(id)));
//        userDetails.put("isDeleted", userIsDeleted(userService.getById(id)));
//        return userDetails;
//    }
//    @RequestMapping("/users")
//    public String getUserDetails(@RequestParam("id") int id, Model model) {
//        List<User> user = userService.getAll();
//        model.addAttribute("isAdmin", userIsAdmin(userService.getById(id)));
//        model.addAttribute("isBlocked", userIsBlocked(userService.getById(id)));
//        model.addAttribute("isDeleted", userIsDeleted(userService.getById(id)));
//        return "adminPanel";
//    }


    @GetMapping("/adminPanel")
    public String showAdminPanelPage(@ModelAttribute("userFilterOptions") UsersFilterDto usersFilterDto,
                                     HttpSession session,
                                     Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("errorMessage", UNAUTHORISED_NOT_LOGGED);
            return "notFound";
        }

        UserFilterOptions userFilterOptions = userFilterMapper.dtoToObject(usersFilterDto);
        List<User> users = userService.filter(userFilterOptions);

        Map<User, Boolean> areAdmins = new HashMap<>();
        Map<User, Boolean> areBlocked = new HashMap<>();
        Map<User, Boolean> areDeleted = new HashMap<>();

        for (User user : users) {
            areAdmins.put(user, userIsAdmin(user));
            areBlocked.put(user, userIsBlocked(user));
            areDeleted.put(user, userIsDeleted(user));
        }
        model.addAttribute("areAdmins", areAdmins);
        model.addAttribute("areBlocked", areBlocked);
        model.addAttribute("areDeleted", areDeleted);

        model.addAttribute("users", users);
        model.addAttribute("userFilterOptions", usersFilterDto);

        return "adminPanel";
    }

    @PostMapping("/{id}/makeAdmin")
    public String makeAdmin(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.makeUserAdmin(id, user);
            model.addAttribute("role", user.getRoles().stream().anyMatch(r -> r.getRoleName().equalsIgnoreCase("admin")));
            model.addAttribute("isAdmin", userIsAdmin(userService.getById(id)));


            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "notFound";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "accessDenied";
        }

    }

}
