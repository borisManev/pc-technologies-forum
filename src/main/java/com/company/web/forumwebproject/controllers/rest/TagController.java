package main.java.com.company.web.forumwebproject.controllers.rest;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.TagMapper;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.tag.TagDtoIn;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.TagService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TagController(TagService tagService, TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Tag> getAll(@RequestParam Optional<String> title) {
        return title.map(s -> List.of(getByTitle(s))).orElseGet(tagService::getAll);
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        try {
            return tagService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private Tag getByTitle(String title) {
        try {
            return tagService.getByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                      @Valid @RequestBody TagDtoIn tagDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Tag tag = tagMapper.dtoToObject(tagDTO);
            tag.setId(id);
            tagService.update(tag, user);
            return tag;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Tag delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Tag tag = getById(id);
            tagService.delete(id, user);
            return tag;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
