package main.java.com.company.web.forumwebproject.controllers.rest;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.PhoneMapper;
import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.PhoneNumberDtoIn;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.PhoneService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/api/phone")
public class PhoneNumberController {
    private final PhoneService phoneService;
    private final PhoneMapper phoneMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhoneNumberController(PhoneService phoneService, PhoneMapper phoneMapper, AuthenticationHelper authenticationHelper) {
        this.phoneService = phoneService;
        this.phoneMapper = phoneMapper;
        this.authenticationHelper = authenticationHelper;
    }
    @GetMapping
    public PhoneNumber getPhoneNumber(@RequestParam Optional<String> number){
    return getByNumber(String.valueOf(number));
    }

    @GetMapping("/{id}")
    public PhoneNumber getByUserId(@PathVariable int id) {
        try {
            return phoneService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private PhoneNumber getByNumber(String number) {
        try {
            return phoneService.getByNumber(number);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public PhoneNumber update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                      @Valid @RequestBody PhoneNumberDtoIn phoneNumberDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PhoneNumber phoneNumber = phoneMapper.dtoToObject(phoneNumberDtoIn);

            phoneService.update(phoneNumber, user);
            return phoneNumber;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public PhoneNumber delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PhoneNumber phoneNumber = getByUserId(id);
            phoneService.delete(id, user);
            return phoneNumber;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
