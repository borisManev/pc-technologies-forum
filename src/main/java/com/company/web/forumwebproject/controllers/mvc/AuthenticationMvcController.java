package main.java.com.company.web.forumwebproject.controllers.mvc;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.UserMapper;
import com.company.web.forumwebproject.models.authModels.LoginDto;
import com.company.web.forumwebproject.models.authModels.RegisterDto;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    private final UserService userService;
    private final AuthenticationHelper authHelper;
    private final UserMapper userMapper;


    public AuthenticationMvcController(UserService userService, AuthenticationHelper authHelper, UserMapper userMapper) {
        this.userService = userService;
        this.authHelper = authHelper;
        this.userMapper = userMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            User user = userService.getByUsername(login.getUsername());
            user.setLastLogin(LocalDateTime.now());
            userService.update(user, user);
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("password", "auth_error", e.getMessage());
            session.removeAttribute("currentUser");
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "register";
        }

        try {
            User user = userMapper.dtoToObject(register);
            userService.create(user);
            session.setAttribute("currentUser", register.getUsername());
            return "redirect:/";
        } catch (EntityDuplicationException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }
}
