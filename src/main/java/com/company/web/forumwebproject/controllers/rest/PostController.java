package main.java.com.company.web.forumwebproject.controllers.rest;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.helpers.AuthenticationHelper;
import com.company.web.forumwebproject.helpers.CommentMapper;
import com.company.web.forumwebproject.helpers.PostMapper;
import com.company.web.forumwebproject.helpers.TagMapper;
import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentDtoIn;
import com.company.web.forumwebproject.models.comment.CommentDtoOut;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostDtoIn;
import com.company.web.forumwebproject.models.post.PostDtoOut;
import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.tag.TagDtoIn;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.services.contracts.CommentService;
import com.company.web.forumwebproject.services.contracts.PostService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final CommentController commentController;
    private final PostService postService;
    private final CommentService commentService;
    private final PostMapper postMapper;
    private final TagMapper tagMapper;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PostController(CommentController commentController, PostService postService, CommentService commentService,
                          PostMapper postMapper, TagMapper tagMapper, CommentMapper commentMapper,
                          AuthenticationHelper authenticationHelper) {
        this.commentController = commentController;
        this.postService = postService;
        this.commentService = commentService;
        this.postMapper = postMapper;
        this.tagMapper = tagMapper;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<PostDtoOut> search(@RequestParam Optional<String> search) {
        try {
            return postService.search(search).stream()
                    .map(postMapper::ObjectToDto)
                    .collect(Collectors.toList());
        } catch (InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<PostDtoOut> filter(
            @RequestParam(required = false) String title,
            @RequestParam(required = false) String content,
            @RequestParam(required = false) String titleAndContent,
            @RequestParam(required = false) String createdBy,
            @RequestParam(required = false, name = "tag") String tagTitle,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) Integer limit
    ) {
        try {
            PostFilterOptions filterOptions = new PostFilterOptions(title, content, titleAndContent,
                    createdBy, tagTitle, sort, limit);
            return postService.filter(filterOptions)
                    .stream()
                    .map(postMapper::ObjectToDto)
                    .collect(Collectors.toList());
        } catch (InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PostDtoOut getById(@PathVariable int id) {
        return postMapper.ObjectToDto(getPostById(id));
    }

    private Post getPostById(int id) {
        try {
            return postService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public PostDtoOut create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDtoIn postDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(postDTO);
            postService.create(post, user);
            return postMapper.ObjectToDto(post);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public PostDtoOut update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                             @Valid @RequestBody PostDtoIn postDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.dtoToObject(postDTO);
            post.setId(id);
            postService.update(post, user);
            post = getPostById(id);
            return postMapper.ObjectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public PostDtoOut delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = getPostById(id);
            postService.delete(id, user);
            return postMapper.ObjectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/like")
    public PostDtoOut like(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = getPostById(id);
            postService.likePost(post, user);
            return postMapper.ObjectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/dislike")
    public PostDtoOut dislike(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = getPostById(id);
            postService.dislikePost(post, user);
            return postMapper.ObjectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    //TAGS
    //---------------------------------------------------------------------------------------------------------------

    @GetMapping("/{id}/tags")
    public Set<Tag> getTags(@PathVariable int id) {
        return getPostById(id).getTags().orElse(null);
    }

//    @GetMapping("/{postId}/tags/{tagId}")
//    public Tag getTags(@PathVariable int postId, @PathVariable int tagId) {
//        Post post = getPostById(postId);
//        if (post.getTags().isEmpty()) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
//                    String.format("Post with id %d has no tags.", postId));
//        } else {
//            return post.getTags().get().stream()
//                    .filter(t -> t.getId() == tagId)
//                    .findFirst()
//                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
//                            String.format("Post with id %d has no tag with id %d.", postId, tagId)));
//        }
//    }

//    @GetMapping("/{id}/tags")
//    public Tag getTags(@PathVariable int id, @RequestParam(name = "tag") String tagTitle) {
//        Post post = getPostById(id);
//        if (post.getTags().isEmpty()) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
//                    String.format("Post with id %d has no tags.", id));
//        } else {
//            return post.getTags().get().stream()
//                    .filter(t -> t.getTitle().equals(tagTitle))
//                    .findFirst()
//                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
//                            String.format("Post with id %d has no tag with title %s.", id, tagTitle)));
//        }
//    }

    @PostMapping("/{id}/tags")
    public PostDtoOut createTag(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                @Valid @RequestBody TagDtoIn tagDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = getPostById(id);
            Tag tag = tagMapper.dtoToObject(tagDTO);
            postService.addTagToPost(post, tag, user);
            return postMapper.ObjectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicationException | InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{postId}/tags/{tagId}")
    public PostDtoOut deleteTag(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                @PathVariable int tagId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = getPostById(postId);
            postService.removeTagFromPost(post, tagId, user);
            return postMapper.ObjectToDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


//    @DeleteMapping("/{postId}/tags")
//    public PostDTOOut deleteTag(@RequestHeader HttpHeaders headers, @PathVariable int postId,
//                                @RequestParam(name = "tag") String tagTitle) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            Post post = getPostById(postId);
//            postService.removeTagFromPost(post, tagTitle, user);
//            return postMapper.ObjectToDTO(post);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }

    //COMMENTS
    //---------------------------------------------------------------------------------------------------------------

    @GetMapping("/{id}/comments")
    public List<CommentDtoOut> filterComments(@PathVariable int id,
                                              @RequestParam(required = false) String content,
                                              @RequestParam(required = false) String username,
                                              @RequestParam(required = false) String sort) {
        //verify that a post with given id exists
        getPostById(id);
        return commentController.filter(content, username, id, sort);
    }

    @PostMapping("/{id}/comments")
    public CommentDtoOut createComment(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                       @Valid @RequestBody CommentDtoIn commentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(commentDto);
            Post post = getPostById(id);
            commentService.create(post, comment, user);
            return commentMapper.ObjectToDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/comments/{commentId}")
    public CommentDtoOut updateComment(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                       @Valid @RequestBody CommentDtoIn commentDto, @PathVariable int commentId) {
        verifyCommentBelongsToPost(postId, commentId);
        return commentController.update(headers, commentId, commentDto);
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public CommentDtoOut deleteComment(@RequestHeader HttpHeaders headers, @PathVariable int postId,
                                       @PathVariable int commentId) {
        verifyCommentBelongsToPost(postId, commentId);
        return commentController.delete(headers, commentId);
    }

    private void verifyCommentBelongsToPost(int postId, int commentId) {
        getPostById(postId).getComments()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.CONFLICT,
                        format("Post with id %d has no comments.", postId)))
                .stream()
                .filter(comment -> comment.getId() == commentId)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.CONFLICT,
                        format("Post with id %d has no comment with id %d.", postId, commentId)));
    }
}