package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.user.UserFilterOptions;
import com.company.web.forumwebproject.models.user.UsersFilterDto;
import org.springframework.stereotype.Component;

@Component
public class UserFilterMapper {

    public UserFilterOptions dtoToObject(UsersFilterDto filterDto) {
        UserFilterOptions filterOptions = new UserFilterOptions();
        if (filterDto.getSortBy() != null && !filterDto.getSortBy().isBlank()) {
            filterOptions.setSortBy(filterDto.getSortBy());
        }
        if (filterDto.getSortOrder() != null && !filterDto.getSortOrder().isBlank()) {
            filterOptions.setSortOrder(filterDto.getSortOrder());
        }
        if (filterDto.getUsername() != null && !filterDto.getUsername().isBlank()) {
            filterOptions.setUsername(filterDto.getUsername());
        }
        if (filterDto.getFirstName() != null && !filterDto.getFirstName().isBlank()) {
            filterOptions.setFirstName(filterDto.getFirstName());
        }
        if (filterDto.getLastName() != null && !filterDto.getLastName().isBlank()) {
            filterOptions.setLastName(filterDto.getLastName());
        }
        if (filterDto.getEmail() != null && !filterDto.getEmail().isBlank()) {
            filterOptions.setEmail(filterDto.getEmail());
        }
        return filterOptions;
    }
}
