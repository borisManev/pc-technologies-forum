package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.models.comment.CommentFilterOptionsDto;
import org.springframework.stereotype.Component;

@Component
public class CommentFilterMapper {
    public CommentFilterOptions dtoToObject(CommentFilterOptionsDto filterDto) {
        CommentFilterOptions filterOptions = new CommentFilterOptions();
        if (filterDto.getSortBy() != null && !filterDto.getSortBy().isBlank()) {
            if (filterDto.getSortOrder() != null && !filterDto.getSortOrder().isBlank()) {
                filterOptions.setSort(filterDto.getSortBy() + '_' + filterDto.getSortOrder());
            } else {
                filterOptions.setSort(filterDto.getSortBy() + '_' + "asc");
            }
        }
        if (filterDto.getKeyword() != null && !filterDto.getKeyword().isBlank()) {
            filterOptions.setContent(filterDto.getKeyword());
        }
        return filterOptions;
    }
}
