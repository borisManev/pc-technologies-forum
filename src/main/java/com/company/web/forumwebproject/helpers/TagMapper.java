package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.tag.TagDtoIn;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    public Tag dtoToObject(TagDtoIn tagDTO) {
        Tag tag = new Tag();
        tag.setTitle(tagDTO.getTitle());
        tag.setDescription(tagDTO.getDescription());
        return tag;
    }
}
