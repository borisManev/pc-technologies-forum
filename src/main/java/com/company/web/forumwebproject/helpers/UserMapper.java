package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.authModels.ProfileUpdateDto;
import com.company.web.forumwebproject.models.authModels.RegisterDto;
import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserDtoIn;
import com.company.web.forumwebproject.models.user.UserDtoOut;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import com.company.web.forumwebproject.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {
    @Autowired
    private RoleRepository repository;
    @Autowired
    private UserRepository userRepository;

    public User dtoToObject(UserDtoIn userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        return user;
    }

    public User dtoToObject(RegisterDto registerDto) {
        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());

        return user;
    }

    public UserDtoOut ObjectToDto(User user) {
        UserDtoOut userDto = new UserDtoOut();

        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        if (!user.getRoles().isEmpty()) {
            userDto.setRoles(getRoleNames(user.getRoles()));
        }
        if (user.getRoles().isEmpty()) {
            userDto.setRoles(Collections.singletonList(repository.getById(1).getRoleName()));
        }

        return userDto;
    }

    public UserDtoIn UserToDTOIn(User user) {
        UserDtoIn userDtoIn = new UserDtoIn();

        userDtoIn.setUsername(user.getUsername());
        userDtoIn.setFirstName(user.getFirstName());
        userDtoIn.setLastName(user.getLastName());
        userDtoIn.setPassword(user.getPassword());
        userDtoIn.setEmail(user.getEmail());

        if (user.getPhoneNumber().isPresent()) {
            userDtoIn.setPhoneNumber((user.getPhoneNumber()).get().getPhoneNumber());
        }

        return userDtoIn;
    }

    public ProfileUpdateDto userToDtoUpdate(User user) {
        ProfileUpdateDto updateDto = new ProfileUpdateDto();

        updateDto.setFirstName(user.getFirstName());
        updateDto.setLastName(user.getLastName());
        updateDto.setEmail(user.getEmail());
        if (user.getPhoneNumber().isPresent()) {
            updateDto.setPhoneNumber((user.getPhoneNumber().get()).getPhoneNumber());
        }

        return updateDto;
    }

    public User fromDto(UserDtoIn userDtoIn, int id) {
        User user = userRepository.getById(id);


        user.setUsername(user.getUsername());
        user.setFirstName(userDtoIn.getFirstName());
        user.setLastName(userDtoIn.getLastName());
        user.setPassword(userDtoIn.getPassword());
        user.setEmail(userDtoIn.getEmail());
        user.setPhoneNumber(new PhoneNumber(id, userDtoIn.getPhoneNumber()));
        if (userDtoIn.getPhoneNumber() == null || userDtoIn.getPhoneNumber().isEmpty()) {
            user.setPhoneNumber(new PhoneNumber(id, null));
        } else {
            user.setPhoneNumber(new PhoneNumber(id, userDtoIn.getPhoneNumber()));

        }

        return user;
    }

    public User fromDto(ProfileUpdateDto userDtoIn, int id) {
        User user = userRepository.getById(id);

        user.setFirstName(userDtoIn.getFirstName());
        user.setLastName(userDtoIn.getLastName());
        user.setPassword(userDtoIn.getNewPassword());
        user.setEmail(userDtoIn.getEmail());
        if (userDtoIn.getPhoneNumber() == null || userDtoIn.getPhoneNumber().isEmpty()) {
            user.setPhoneNumber(null);
        } else {
            user.setPhoneNumber(new PhoneNumber(id, userDtoIn.getPhoneNumber()));
        }
        return user;
    }

    public UserDtoOut ObjectToDtoOut(User user) {
        UserDtoOut userDto = new UserDtoOut();

        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setCreation(user.getCreation());
        userDto.setLastLogin(user.getLastLogin());


        if (!user.getRoles().isEmpty()) {
            userDto.setRoles(getRoleNames(user.getRoles()));
        }
        if (user.getRoles().isEmpty()) {
            userDto.setRoles(Collections.singletonList(repository.getById(1).getRoleName()));
        }

        return userDto;
    }

    private List<String> getRoleNames(List<Role> roles) {
        return roles.stream()
                .map(Role::getRoleName)
                .collect(Collectors.toList());
    }
}
