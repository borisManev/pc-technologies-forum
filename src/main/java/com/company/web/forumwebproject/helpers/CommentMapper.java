package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentDtoIn;
import com.company.web.forumwebproject.models.comment.CommentDtoOut;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {
    public Comment dtoToObject(CommentDtoIn commentDto) {
        Comment comment = new Comment();
        comment.setContent(commentDto.getContent());
        return comment;
    }

    public CommentDtoOut ObjectToDto(Comment comment) {
        CommentDtoOut commentDto = new CommentDtoOut();
        commentDto.setContent(comment.getContent());
        commentDto.setLikes(comment.getLikes() - comment.getDislikes());
        commentDto.setCreatedBy(comment.getCreatedBy().getUsername());
        commentDto.setCreation(comment.getCreation());
        commentDto.setLastUpdate(comment.getLastUpdate());
        return commentDto;
    }
}
