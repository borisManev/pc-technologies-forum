package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.models.post.PostFilterOptionsDto;
import org.springframework.stereotype.Component;

@Component
public class PostFilterMapper {

    public PostFilterOptions dtoToObject(PostFilterOptionsDto filterDto) {
        PostFilterOptions filterOptions = new PostFilterOptions();
        if (filterDto.getSortBy() != null && !filterDto.getSortBy().isBlank()) {
            if (filterDto.getSortOrder() != null && !filterDto.getSortOrder().isBlank()) {
                filterOptions.setSort(filterDto.getSortBy() + '_' + filterDto.getSortOrder());
            } else {
                filterOptions.setSort(filterDto.getSortBy() + '_' + "asc");
            }
        }
        if (filterDto.getKeyword() != null && !filterDto.getKeyword().isBlank()) {
            filterOptions.setTitleAndContent(filterDto.getKeyword());
        }
        if (filterDto.getCreatedBy() != null && !filterDto.getCreatedBy().isBlank()) {
            filterOptions.setCreatedBy(filterDto.getCreatedBy());
        }
        if (filterDto.getTag() != null && !filterDto.getTag().isBlank()) {
            filterOptions.setTagTitle(filterDto.getTag());
        }
        return filterOptions;
    }
}
