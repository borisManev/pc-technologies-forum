package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.models.role.RoleDtoIn;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {

    public Role dtoToObject(RoleDtoIn roleDTO) {
        Role role = new Role();
        role.setRoleName(roleDTO.getRoleName());
        return role;
    }
}