package main.java.com.company.web.forumwebproject.helpers;

import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostDtoIn;
import com.company.web.forumwebproject.models.post.PostDtoOut;
import com.company.web.forumwebproject.models.tag.Tag;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PostMapper {

    public Post dtoToObject(PostDtoIn postDTO) {
        Post post = new Post();
        post.setTitle(postDTO.getTitle());
        post.setContent(postDTO.getContent());
        return post;
    }

    public PostDtoOut ObjectToDto(Post post) {
        PostDtoOut postDTO = new PostDtoOut();

        postDTO.setTitle(post.getTitle());
        postDTO.setContent(post.getContent());
        postDTO.setLikes(post.getLikes() - post.getDislikes());
        postDTO.setCreatedBy(post.getCreatedBy().getUsername());
        postDTO.setCreation(post.getCreation());
        postDTO.setLastUpdate(post.getLastUpdate());
        if (post.getTags().isPresent()) {
            postDTO.setTags(getTagTitles(post.getTags().get()));
        }
        return postDTO;
    }

    private List<String> getTagTitles(Set<Tag> tags) {
        return tags.stream()
                .map(Tag::getTitle)
                .collect(Collectors.toList());
    }
}
