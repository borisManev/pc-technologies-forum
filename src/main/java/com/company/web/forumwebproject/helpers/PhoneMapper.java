package main.java.com.company.web.forumwebproject.helpers;


import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.PhoneNumberDtoIn;
import org.springframework.stereotype.Component;
@Component
public class PhoneMapper {
    public PhoneNumber dtoToObject(PhoneNumberDtoIn phoneNumberDtoIn) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(phoneNumberDtoIn.getNumber());

        return phoneNumber;
    }
}
