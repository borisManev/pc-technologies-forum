package main.java.com.company.web.forumwebproject.exceptions;

public class EntityDuplicationException extends RuntimeException {
    public EntityDuplicationException(String message) {
        super(message);
    }
}
