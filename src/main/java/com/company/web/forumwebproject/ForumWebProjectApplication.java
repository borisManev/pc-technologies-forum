package com.company.web.forumwebproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForumWebProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForumWebProjectApplication.class, args);
    }
}
