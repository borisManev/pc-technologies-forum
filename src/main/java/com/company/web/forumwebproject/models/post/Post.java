package main.java.com.company.web.forumwebproject.models.post;

import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.contracts.Creatable;
import com.company.web.forumwebproject.models.contracts.Identifiable;
import com.company.web.forumwebproject.models.reactions.UserPostReaction;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "posts")
public class Post implements Identifiable, Creatable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "creation")
    private LocalDateTime creation;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User createdBy;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "posts_tags",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @OneToMany(mappedBy = "postedOn", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private Set<Comment> comments;

    @OneToMany(mappedBy = "reactedOnId", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private Set<UserPostReaction> userPostReactions;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLikes() {
        return userPostReactions == null ? 0 :
                (int) userPostReactions.stream().filter(UserPostReaction::isLiked).count();
    }

    public int getDislikes() {
        return userPostReactions == null ? 0 : userPostReactions.size() - getLikes();
    }

    public int getRating() {
        if (userPostReactions == null) return 0;
        int rating = getLikes();
        int dislikes = userPostReactions.size() - rating;
        rating -= dislikes;
        return rating;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Optional<Set<Tag>> getTags() {
        return Optional.ofNullable(tags);
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Optional<Set<Comment>> getComments() {
        return Optional.ofNullable(comments);
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Optional<Set<UserPostReaction>> getUserPostReactions() {
        return Optional.ofNullable(userPostReactions);
    }

    public void setUserPostReactions(Set<UserPostReaction> userPostReactions) {
        this.userPostReactions = userPostReactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return id == post.id && title.equals(post.title) && content.equals(post.content) && Objects.equals(creation, post.creation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, creation);
    }
}