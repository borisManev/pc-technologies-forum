package main.java.com.company.web.forumwebproject.models.contracts;

public interface Identifiable {
    int getId();
}
