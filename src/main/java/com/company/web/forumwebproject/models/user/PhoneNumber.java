package main.java.com.company.web.forumwebproject.models.user;

import com.company.web.forumwebproject.models.contracts.Identifiable;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "phone_numbers")
public class PhoneNumber implements Identifiable {


    @Id
    @Column(name = "user_id")
    private int userId;

    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "user_id")
    private User user;

    public PhoneNumber() {
    }

    public PhoneNumber( String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }
    public PhoneNumber(int userId, String phoneNumber) {
        this.userId = userId;
        this.phoneNumber = phoneNumber;
    }
    @Override
    public int getId() {
        return userId;
    }

    public void setId(int id) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber that = (PhoneNumber) o;
        return userId == that.userId && phoneNumber.equals(that.phoneNumber) && user.equals(that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, phoneNumber, user);
    }
}
