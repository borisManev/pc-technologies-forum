package main.java.com.company.web.forumwebproject.models.user;

import com.company.web.forumwebproject.models.role.Role;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class UserFilterOptions {

    private String username;
    private String firstName;
    private String lastName;
    private String email;

    private String sortBy;
    private String sortOrder;
    private Boolean isAdmin;
    private Boolean isDeleted;
    private Boolean isBlocked;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private String phoneNumber;
    private LocalDateTime lastLogin;
    private LocalDateTime creation;
    private List<Role> roles;

    public UserFilterOptions() {
        this(null, null, null, null,
                null, null, null, null);
    }

    public UserFilterOptions(String username,
                             String firstName,
                             String lastName,
                             String email,
                             String sortBy,
                             String sortOrder,
                             String phoneNumber,
                             List<Role> roles

    ) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
        this.phoneNumber = phoneNumber;
        this.roles = roles;


//        this.lastLogin = Optional.ofNullable(lastLogin);
//        this.creation = Optional.ofNullable(creation);
    }

    public UserFilterOptions(String username,
                             String firstName,
                             String lastName,
                             String email,
                             String sortBy,
                             String sortOrder,
                             String phoneNumber,
                             boolean isAdmin,
                             boolean isDeleted,
                             boolean isBlocked
    ) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.sortBy = sortBy;
        this.sortOrder =sortOrder;
        this.phoneNumber = phoneNumber;
        this.isAdmin = isAdmin;
        this.isDeleted = isDeleted;
        this.isBlocked = isBlocked;

//        this.lastLogin = Optional.ofNullable(lastLogin);
//        this.creation = Optional.ofNullable(creation);
    }

    public Optional<String> getUsername() {
        return Optional.ofNullable(username);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Optional<String> getFirstName() {
        return Optional.ofNullable(firstName);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Optional<String> getLastName() {
        return Optional.ofNullable(lastName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Optional<String> getSortBy() {
        return Optional.ofNullable(sortBy);
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return Optional.ofNullable(sortOrder);
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Optional<Boolean> getAdmin() {
        return Optional.ofNullable(isAdmin);
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Optional<Boolean> getDeleted() {
        return Optional.ofNullable(isDeleted);
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Optional<Boolean> getBlocked() {
        return Optional.ofNullable(isBlocked);
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public Optional<LocalDateTime> getLastLogin() {
        return Optional.ofNullable(lastLogin);
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Optional<LocalDateTime> getCreation() {
        return Optional.ofNullable(creation);
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public Optional<List<Role>> getRoles() {
        return Optional.ofNullable(roles);
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

// getLastLogin() {
//        return lastLogin;
//    }
//
//    public void setLastLogin(Optional<LocalDateTime> lastLogin) {
//        this.lastLogin = lastLogin;
//    }
//
//    public Optional<LocalDateTime> getCreation() {
//        return creation;
//    }
//
//    public void setCreation(Optional<LocalDateTime> creation) {
//        this.creation = creation;
//    }
}

