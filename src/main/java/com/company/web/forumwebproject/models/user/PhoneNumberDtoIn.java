package main.java.com.company.web.forumwebproject.models.user;


import jakarta.validation.constraints.Size;

public class PhoneNumberDtoIn {


    @Size(min = 4, max = 16)
    private String number;


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
