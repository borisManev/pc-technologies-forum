package main.java.com.company.web.forumwebproject.models.post;

import java.util.Optional;

public class PostFilterOptions {
    String title;
    String content;
    String titleAndContent;
    String createdBy;
    String tagTitle;
    String sort;
    Integer limit;

    public PostFilterOptions() {
    }

    public PostFilterOptions(String title, String content, String titleAndContent, String createdBy, String tagTitle,
                             String sort, Integer limit) {
        this.title = title;
        this.content = content;
        this.titleAndContent = titleAndContent;
        this.createdBy = createdBy;
        this.tagTitle = tagTitle;
        this.sort = sort;
        this.limit = limit;
    }

    public Optional<String> getTitle() {
        return Optional.ofNullable(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Optional<String> getContent() {
        return Optional.ofNullable(content);
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Optional<String> getTitleAndContent() {
        return Optional.ofNullable(titleAndContent);
    }

    public void setTitleAndContent(String titleOrContent) {
        this.titleAndContent = titleOrContent;
    }

    public Optional<String> getCreatedBy() {
        return Optional.ofNullable(createdBy);
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Optional<String> getTagTitle() {
        return Optional.ofNullable(tagTitle);
    }

    public void setTagTitle(String tagTitle) {
        this.tagTitle = tagTitle;
    }

    public Optional<String> getSort() {
        return Optional.ofNullable(sort);
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Optional<Integer> getLimit() {
        return Optional.ofNullable(limit);
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
