package main.java.com.company.web.forumwebproject.models.contracts;

import com.company.web.forumwebproject.models.user.User;

public interface Creatable {

    User getCreatedBy();
}
