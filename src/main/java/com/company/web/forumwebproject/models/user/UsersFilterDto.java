package main.java.com.company.web.forumwebproject.models.user;

import com.company.web.forumwebproject.models.role.Role;

import java.util.List;

public class UsersFilterDto {
    private String username;
    private String firstName;
    private String lastName;


    private String email;
    private String sortBy;
    private String sortOrder;
    private String phoneNumber;
    private List<Role> roles;
//    private LocalDateTime lastLogin;
//    private LocalDateTime creation;

    public UsersFilterDto() {
    }

    public UsersFilterDto(String username, String firstName, String lastName,
                          String email, String phoneNumber, String sortBy,
                          String sortOrder, List<Role> roles) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
        this.phoneNumber = phoneNumber;
        this.roles = roles;

//        this.lastLogin = lastLogin;
//        this.creation = creation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    //    public LocalDateTime getLastLogin() {
//        return lastLogin;
//    }
//
//    public void setLastLogin(LocalDateTime lastLogin) {
//        this.lastLogin = lastLogin;
//    }
//
//    public LocalDateTime getCreation() {
//        return creation;
//    }
//
//    public void setCreation(LocalDateTime creation) {
//        this.creation = creation;
//    }
}
