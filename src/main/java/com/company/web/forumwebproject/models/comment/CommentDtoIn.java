package main.java.com.company.web.forumwebproject.models.comment;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class CommentDtoIn {
    @NotEmpty(message = "Content can not be empty.")
    @Size(message = "Content should be between 4 and 32 symbols.", min = 4, max = 1024)
    private String content;

    public CommentDtoIn() {
    }

    public CommentDtoIn(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
