package main.java.com.company.web.forumwebproject.models.role;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class RoleDtoIn {

    @NotNull
    @Size(min = 4, max = 16)
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
