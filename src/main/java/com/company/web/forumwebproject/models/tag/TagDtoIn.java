package main.java.com.company.web.forumwebproject.models.tag;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class TagDtoIn {

    @NotNull
    @Size(min = 4, max = 16)
    private String title;

    @Size(min = 8, max = 64)
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
