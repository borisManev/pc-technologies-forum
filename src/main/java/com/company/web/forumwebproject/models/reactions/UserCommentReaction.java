package main.java.com.company.web.forumwebproject.models.reactions;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "users_comments_reactions")
@IdClass(ReactionId.class)
public class UserCommentReaction {

    @Id
    @Column(name = "user_id")
    private int userId;
    @Id
    @Column(name = "comment_id")
    private int reactedOnId;

    @Column(name = "is_liked")
    private boolean isLiked;

    public UserCommentReaction() {
    }

    public UserCommentReaction(int userId, int reactedOnId, boolean isLiked) {
        this.userId = userId;
        this.reactedOnId = reactedOnId;
        this.isLiked = isLiked;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCommentId() {
        return reactedOnId;
    }

    public void setCommentId(int postId) {
        this.reactedOnId = postId;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCommentReaction that = (UserCommentReaction) o;
        return userId == that.userId && reactedOnId == that.reactedOnId && isLiked == that.isLiked;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, reactedOnId, isLiked);
    }
}
