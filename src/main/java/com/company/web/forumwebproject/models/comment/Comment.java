package main.java.com.company.web.forumwebproject.models.comment;

import com.company.web.forumwebproject.models.contracts.Creatable;
import com.company.web.forumwebproject.models.contracts.Identifiable;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.reactions.UserCommentReaction;
import com.company.web.forumwebproject.models.user.User;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "comments")
public class Comment implements Identifiable, Creatable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int id;

    @Column(name = "content")
    private String content;

    @Column(name = "creation")
    private LocalDateTime creation;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User createdBy;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "post_id")
    private Post postedOn;

    @OneToMany(mappedBy = "reactedOnId", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private Set<UserCommentReaction> userCommentReactions;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getLikes() {
        return userCommentReactions == null ? 0 :
                (int) userCommentReactions.stream().filter(UserCommentReaction::isLiked).count();
    }

    public int getDislikes() {
        return userCommentReactions == null ? 0 : userCommentReactions.size() - getLikes();
    }

    public int getRating() {
        if (userCommentReactions == null) return 0;
        int rating = getLikes();
        int dislikes = userCommentReactions.size() - rating;
        rating -= dislikes;
        return rating;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Post getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(Post postedOn) {
        this.postedOn = postedOn;
    }

    public Optional<Set<UserCommentReaction>> getUserCommentReactions() {
        return Optional.ofNullable(userCommentReactions);
    }

    public void setUserCommentReactions(Set<UserCommentReaction> userCommentReactions) {
        this.userCommentReactions = userCommentReactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id && content.equals(comment.content) && Objects.equals(creation, comment.creation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content, creation);
    }
}
