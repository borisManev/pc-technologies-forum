package main.java.com.company.web.forumwebproject.models.comment;

import java.util.Optional;

public class CommentFilterOptions {

    String content;
    String username;
    Integer postId;
    String sort;

    public CommentFilterOptions() {
    }

    public CommentFilterOptions(String content, String username, Integer postId, String sort) {
        this.content = content;
        this.username = username;
        this.postId = postId;
        this.sort = sort;
    }

    public Optional<String> getContent() {
        return Optional.ofNullable(content);
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Optional<String> getUsername() {
        return Optional.ofNullable(username);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Optional<Integer> getPostId() {
        return Optional.ofNullable(postId);
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Optional<String> getSort() {
        return Optional.ofNullable(sort);
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
