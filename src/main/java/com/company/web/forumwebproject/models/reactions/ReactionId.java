package main.java.com.company.web.forumwebproject.models.reactions;

import java.io.Serializable;
import java.util.Objects;

public class ReactionId implements Serializable {
    private int userId;
    private int reactedOnId;

    public ReactionId() {
    }

    public ReactionId(int userId, int reactedOnId) {
        this.userId = userId;
        this.reactedOnId = reactedOnId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReactionId that = (ReactionId) o;
        return userId == that.userId && reactedOnId == that.reactedOnId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, reactedOnId);
    }
}
