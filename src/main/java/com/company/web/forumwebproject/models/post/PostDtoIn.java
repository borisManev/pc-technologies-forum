package main.java.com.company.web.forumwebproject.models.post;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class PostDtoIn {

    @NotEmpty(message = "Title can not be empty.")
    @Size(message = "Title should be between 16 and 64 symbols.", min = 16, max = 64)
    private String title;

    @NotEmpty(message = "Post content can not be empty.")
    @Size(message = "Post content should be between 32 and 8192 symbols.", min = 32, max = 8192)
    private String content;

    public PostDtoIn() {
    }

    public PostDtoIn(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
