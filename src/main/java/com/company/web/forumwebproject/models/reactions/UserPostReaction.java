package main.java.com.company.web.forumwebproject.models.reactions;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "users_posts_reactions")
@IdClass(ReactionId.class)
public class UserPostReaction {

    @Id
    @Column(name = "user_id")
    private int userId;
    @Id
    @Column(name = "post_id")
    private int reactedOnId;

    @Column(name = "is_liked")
    private boolean isLiked;

    public UserPostReaction() {
    }

    public UserPostReaction(int userId, int reactedOnId, boolean isLiked) {
        this.userId = userId;
        this.reactedOnId = reactedOnId;
        this.isLiked = isLiked;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPostId() {
        return reactedOnId;
    }

    public void setPostId(int postId) {
        this.reactedOnId = postId;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPostReaction that = (UserPostReaction) o;
        return userId == that.userId && reactedOnId == that.reactedOnId && isLiked == that.isLiked;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, reactedOnId, isLiked);
    }
}
