package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostFilterOptions;

import java.util.List;
import java.util.Optional;

public interface PostRepository {
    List<Post> search(Optional<String> search);

    List<Post> filter(PostFilterOptions filterOptions);

    Post getById(int id);

    Post create(Post post);

    void update(Post post);

    void delete(int id);

    int getNumOfPosts();
}
