package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Comment> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Comment> list = session.createQuery(" from Comment where " +
                    " content like :keyword ");
            list.setParameter("keyword", "%" + search.get() + "%");
            return list.list();
        }
    }

    private List<Comment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(" from Comment ", Comment.class).list();
        }
    }

    @Override
    public List<Comment> filter(CommentFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Comment c ");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> filter = new ArrayList<>();

            filterOptions.getContent().ifPresent(val -> {
                filter.add(" c.content like :contentKeyword ");
                queryParams.put("contentKeyword", "%" + val + "%");
            });

            filterOptions.getUsername().ifPresent(val -> {
                filter.add(" c.createdBy.username = :username ");
                queryParams.put("username", val);
            });

            filterOptions.getPostId().ifPresent(val -> {
                filter.add(" c.postedOn.id = :postId ");
                queryParams.put("postId", val);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            filterOptions.getSort().ifPresent(val -> queryString.append(generateStringFromSort(val, queryString)));

            return session.createQuery(queryString.toString(), Comment.class).setProperties(queryParams).list();
        }
    }

    private String generateStringFromSort(String sort, StringBuilder queryString) {
        if (sort.isEmpty() || sort.charAt(0) == '_' || sort.charAt(sort.length() - 1) == '_') {
            throw new InvalidParameterException(
                    "Sort should have the following format: {sortBy}_{sortOrder}.");
        }

        String[] params = sort.split("_");

        if (params.length != 2) {
            throw new InvalidParameterException(
                    "Sort should have the following format: {sortBy}_{sortOrder}.");
        }

        StringBuilder sortString = new StringBuilder();
        boolean sortingByRating = false;

        switch (params[0]) {

            case "rating" -> {
                sortingByRating = true;
                queryString.insert(16, " left join c.userCommentReactions u ");
                sortString.append(" group by c.id ");
                sortString.append(" order by sum(case when u.isLiked then 1 when u.isLiked=false then -1 else 0 end) ");
            }
            case "creation" -> sortString.append(" order by c.creation ");
            case "update" -> sortString.append(" order by c.lastUpdate ");
            default -> throw new InvalidParameterException(
                    "Sort by parameter is not supported.");
        }

        switch (params[1]) {
            case "asc" -> {
            }
            case "desc" -> sortString.append(" desc ");
            default -> throw new InvalidParameterException(
                    "Sort order parameter is not supported.");
        }
        if (sortingByRating) {
            //if comments have equal rating the comments with most reactions come out on top
            sortString.append(" ,count(u) desc ");
        }
        return sortString.toString();
    }


    @Override
    public Comment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Comment comment = session.get(Comment.class, id);
            if (comment == null) {
                throw new EntityNotFoundException("Comment", id);
            }
            return comment;
        }
    }

    @Override
    public void create(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Comment comment = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(comment);
            session.getTransaction().commit();
        }
    }
}