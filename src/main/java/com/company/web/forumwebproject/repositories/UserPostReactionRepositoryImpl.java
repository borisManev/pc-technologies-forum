package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.models.reactions.UserPostReaction;
import com.company.web.forumwebproject.repositories.contracts.UserPostReactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserPostReactionRepositoryImpl implements UserPostReactionRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserPostReactionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserPostReaction get(int userId, int postId) {
        try (Session session = sessionFactory.openSession()) {
            List<UserPostReaction> userPostReactions = session.createQuery(
                            "from UserPostReaction where userId = :userId and reactedOnId = :postId",
                            UserPostReaction.class)
                    .setParameter("userId", userId)
                    .setParameter("postId", postId)
                    .list();
            if (userPostReactions.isEmpty()) {
                throw new EntityNotFoundException("UserPostReaction", "userId", String.valueOf(userId));
            }
            return userPostReactions.get(0);
        }
    }

    @Override
    public void create(UserPostReaction userPostReaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(userPostReaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(UserPostReaction userPostReaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(userPostReaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int userId, int postId) {
        UserPostReaction userPostReaction = get(userId, postId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(userPostReaction);
            session.getTransaction().commit();
        }
    }
}
