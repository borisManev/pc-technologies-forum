package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.role.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(int id);

    Role getByRole(String title);

    void create(Role role);

    void update(Role role);

    void delete(int id);
}
