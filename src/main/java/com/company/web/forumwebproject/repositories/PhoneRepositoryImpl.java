package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.repositories.contracts.PhoneRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class PhoneRepositoryImpl implements PhoneRepository {
    private final SessionFactory sessionFactory;

    public PhoneRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PhoneNumber> getAll() {
        try (Session session= sessionFactory.openSession()){
            return session.createQuery("from PhoneNumber",PhoneNumber.class).list();
        }

    }

    @Override
    public PhoneNumber getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PhoneNumber phoneNumber = session.get(PhoneNumber.class, id);
            if (phoneNumber == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            return phoneNumber;
        }
    }

    @Override
    public PhoneNumber getByNumber(String title) {
        try (Session session = sessionFactory.openSession()) {
            List<PhoneNumber> phoneNumbers = session.createQuery("from PhoneNumber " +
                            "where phoneNumber = :phonenumber", PhoneNumber.class)
                    .setParameter("title", title).list();
            if (phoneNumbers.isEmpty()) {
                throw new EntityNotFoundException("Tag", "title", title);
            }
            return phoneNumbers.get(0);
        }
    }

    @Override
    public void create(PhoneNumber phoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(phoneNumber);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(PhoneNumber phoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(phoneNumber);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        PhoneNumber phoneNumber = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(phoneNumber);
            session.getTransaction().commit();
        }
    }
}
