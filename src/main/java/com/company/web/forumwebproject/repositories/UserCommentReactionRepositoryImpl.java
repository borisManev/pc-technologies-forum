package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.models.reactions.UserCommentReaction;
import com.company.web.forumwebproject.repositories.contracts.UserCommentReactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserCommentReactionRepositoryImpl implements UserCommentReactionRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserCommentReactionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserCommentReaction get(int userId, int commentId) {
        try (Session session = sessionFactory.openSession()) {
            List<UserCommentReaction> userCommentReactions = session.createQuery(
                            "from UserCommentReaction where userId = :userId and reactedOnId = :commentId",
                            UserCommentReaction.class)
                    .setParameter("userId", userId)
                    .setParameter("commentId", commentId)
                    .list();
            if (userCommentReactions.isEmpty()) {
                throw new EntityNotFoundException("UserCommentReaction", "userId", String.valueOf(userId));
            }
            return userCommentReactions.get(0);
        }
    }

    @Override
    public void create(UserCommentReaction userCommentReaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(userCommentReaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(UserCommentReaction userCommentReaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(userCommentReaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int userId, int commentId) {
        UserCommentReaction userCommentReaction = get(userId, commentId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(userCommentReaction);
            session.getTransaction().commit();
        }
    }
}
