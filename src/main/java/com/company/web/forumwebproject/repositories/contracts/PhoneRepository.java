package main.java.com.company.web.forumwebproject.repositories.contracts;


import com.company.web.forumwebproject.models.user.PhoneNumber;

import java.util.List;

public interface PhoneRepository {
    List<PhoneNumber> getAll();

    PhoneNumber getById(int id);

    PhoneNumber getByNumber(String phoneNumber);

    void create(PhoneNumber phoneNumber);

    void update(PhoneNumber phoneNumber);

    void delete(int id);
}
