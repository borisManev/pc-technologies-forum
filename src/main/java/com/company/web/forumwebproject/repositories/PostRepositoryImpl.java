package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.repositories.contracts.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Post> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Post> list = session.createQuery(" from Post where " +
                    " title like :keyword or content like :keyword");
            list.setParameter("keyword", "%" + search.get() + "%");
            return list.list();
        }
    }

    private List<Post> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(" from Post ", Post.class).list();
        }
    }

    @Override
    public List<Post> filter(PostFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Post p ");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> filter = new ArrayList<>();

            if (filterOptions.getTitleAndContent().isPresent() &&
                    (filterOptions.getTitle().isPresent() || filterOptions.getContent().isPresent())) {
                throw new InvalidParameterException(
                        "Cannot have both filtering by title/content and filtering by title and content.");
            }

            filterOptions.getTitle().ifPresent(val -> {
                filter.add(" p.title like :titleKeyword ");
                queryParams.put("titleKeyword", "%" + val + "%");
            });

            filterOptions.getContent().ifPresent(val -> {
                filter.add(" p.content like :contentKeyword ");
                queryParams.put("contentKeyword", "%" + val + "%");
            });

            filterOptions.getTitleAndContent().ifPresent(val -> {
                filter.add(" (p.title like :keyword or p.content like :keyword) ");
                queryParams.put("keyword", "%" + val + "%");
            });

            filterOptions.getCreatedBy().ifPresent(val -> {
                filter.add(" p.createdBy.username = :username ");
                queryParams.put("username", val);
            });

            filterOptions.getTagTitle().ifPresent(val -> {
                queryString.insert(13, " left join p.tags t ");
                filter.add(" t.title = :tagTitle ");
                queryParams.put("tagTitle", val);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }
            filterOptions.getSort().ifPresent(val -> queryString.append(generateStringFromSort(val, queryString)));

            if (filterOptions.getLimit().isPresent()) {
                int limit = filterOptions.getLimit().get();
                if (limit <= 0) {
                    throw new InvalidParameterException("Limit parameter cannot be negative or zero.");
                }
                return session.createQuery(queryString.toString(), Post.class)
                        .setMaxResults(limit)
                        .setProperties(queryParams)
                        .list();
            } else {
                return session.createQuery(queryString.toString(), Post.class).setProperties(queryParams).list();
            }
        }
    }

    @Override
    public int getNumOfPosts() {
        try (Session session = sessionFactory.openSession()) {
            return Math.toIntExact(session.createQuery(" select count(p) from Post p", Long.class).getSingleResult());
        }
    }

    private String generateStringFromSort(String sort, StringBuilder queryString) {
        if (sort.isEmpty() || sort.charAt(0) == '_' || sort.charAt(sort.length() - 1) == '_') {
            throw new InvalidParameterException(
                    "Sort should have the following format: {sortBy}_{sortOrder}.");
        }

        String[] params = sort.split("_");

        if (params.length != 2) {
            throw new InvalidParameterException(
                    "Sort should have the following format: {sortBy}_{sortOrder}.");
        }

        StringBuilder sortString = new StringBuilder();
        boolean sortingByRating = false;

        switch (params[0]) {
            case "title" -> sortString.append(" order by p.title ");
            case "rating" -> {
                sortingByRating = true;
                queryString.insert(13, " left join p.userPostReactions u ");
                sortString.append(" group by p.id ");
                sortString.append(" order by sum(case when u.isLiked then 1 when u.isLiked=false then -1 else 0 end) ");
            }
            case "commented" -> sortString.append(" order by size(p.comments) ");
            case "creation" -> sortString.append(" order by p.creation ");
            case "update" -> sortString.append(" order by p.lastUpdate ");
            default -> throw new InvalidParameterException(
                    "Sort by parameter is not supported.");
        }

        switch (params[1]) {
            case "asc" -> {
            }
            case "desc" -> sortString.append(" desc ");
            default -> throw new InvalidParameterException(
                    "Sort order parameter is not supported.");
        }
        if (sortingByRating) {
            //if posts have equal rating the posts with most reactions come out on top
            sortString.append(" ,count(u) desc ");
        }
        return sortString.toString();
    }

    @Override
    public Post getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            List<Post> posts = session.createQuery("from Post where id = :id", Post.class)
                    .setParameter("id", id).getResultList();
            if (posts.isEmpty()) {
                throw new EntityNotFoundException("Post", id);
            }
            return posts.get(0);

//            Post post = session.get(Post.class,id);
//            if (post == null) {
//                throw new EntityNotFoundException("Post", id);
//            }
//            return post;
        }
    }

    @Override
    public Post create(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(post);
            session.getTransaction().commit();
            return post;
        }
    }

    @Override
    public void update(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Post post = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(post);
            session.createNativeQuery(" delete from posts_tags where post_id = :postId ")
                    .setParameter("postId", post.getId()).executeUpdate();
            session.getTransaction().commit();
        }
    }
}
