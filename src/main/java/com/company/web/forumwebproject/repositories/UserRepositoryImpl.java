package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserFilterOptions;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import com.company.web.forumwebproject.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, RoleRepository roleRepository) {
        this.sessionFactory = sessionFactory;

    }


    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<User> list = session.createQuery(" from User where " +
                    " username like :keyword or firstName like :keyword  or lastName like :keyword  or email like :keyword");
            list.setParameter("keyword", "%" + search.get() + "%");
            return list.list();
        }
    }


    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            List<User> users = session.createQuery("from User where username = :username", User.class)
                    .setParameter("username", username).list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return users.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            List<User> users = session.createQuery("from User where email = :email", User.class)
                    .setParameter("email", email).list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return users.get(0);
        }
    }

    @Override
    public User getByPhoneNumber(String phoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            List<User> users = session.createQuery("from User where phoneNumber = :phoneNumber", User.class)
                    .setParameter("phoneNumber", phoneNumber).list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException("User", "phone number", phoneNumber);
            }
            return users.get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }


    public Long getNumberOfUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(" select count(user) from User as user", Long.class);

            return query.getSingleResult();
        }
    }

//    @Override
//    public void delete(int id) {
//        User user = getById(id);
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.remove(user);
//            session.createNativeQuery(" delete from posts where user_id = :userId ")
//                    .setParameter("userId", user.getId()).executeUpdate();
//            session.createNativeQuery(" delete from users_posts_reactions where user_id = :userId ")
//                    .setParameter("userId", user.getId()).executeUpdate();
//            session.createNativeQuery(" delete from users_comments_reactions where user_id = :userId ")
//                    .setParameter("userId", user.getId()).executeUpdate();
//            session.createNativeQuery(" delete from users_roles where user_id = :userId ")
//                    .setParameter("userId", user.getId()).executeUpdate();
//            session.getTransaction().commit();
//        }
//    }

    @Override
    public List<User> filter(UserFilterOptions userFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            // StringBuilder queryString = new StringBuilder(" from User u left join u.roles ");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> filter = new ArrayList<>();
            if (userFilterOptions.getUsername().isPresent() && !userFilterOptions.getUsername().get().equals("")) {
                filter.add("username like :username");
                queryParams.put("username", String.format("%%%s%%", userFilterOptions.getUsername().get()));

            }
            if (userFilterOptions.getFirstName().isPresent() && !userFilterOptions.getFirstName().get().equals("")) {
                filter.add("firstName like :firstName");
                queryParams.put("firstName", userFilterOptions.getFirstName().get());
            }
            if (userFilterOptions.getLastName().isPresent() && !userFilterOptions.getLastName().get().equals("")) {
                filter.add("lastName like :lastName");
                queryParams.put("lastName", userFilterOptions.getLastName().get());
            }
            if (userFilterOptions.getEmail().isPresent() && !userFilterOptions.getEmail().get().equals("")) {
                filter.add("email like :email");
                queryParams.put("email", userFilterOptions.getEmail().get());
            }


            StringBuilder queryString = new StringBuilder("from User");
            if (!filter.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filter));
            }

            queryString.append(generateOrderBy(userFilterOptions));

            return session.createQuery(queryString.toString(), User.class).setProperties(queryParams).list();
        }

    }

    private String generateOrderBy(UserFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String sortBy = "";
        StringBuilder sorting = new StringBuilder();
        switch (filterOptions.getSortBy().get()) {
            case "username":
                sortBy = "username";
                break;
            case "firstName":
                sortBy = "firstName";
                break;
            case "lastName":
                sortBy = "lastName";
                break;
            case "email":
                sortBy = "email";
                break;
            case "Sort by":
                break;
        }
        if (!sortBy.equals("")) {
            sortBy = String.format(" order by %s", sortBy);
        }


        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            sortBy = String.format("%s desc", sortBy);

        }

        sorting.append(sortBy);
        return sorting.toString();
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from User", User.class).list();
        }
    }


//    @Override
//    public void block(int id, Role role) {
//
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.merge(user);
//            session.getTransaction().commit();
//        }
//    }

    //    @Override
//    public void unblock(int id, Role role) {
//        User user = getById(id);
//
//        try (Session session = sessionFactory.openSession()) {
//            session.beginTransaction();
//            session.merge(user);
//            session.getTransaction().commit();
//        }
//    }

}
