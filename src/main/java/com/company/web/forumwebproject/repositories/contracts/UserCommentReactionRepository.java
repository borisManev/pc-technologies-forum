package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.reactions.UserCommentReaction;

public interface UserCommentReactionRepository {
    UserCommentReaction get(int userId, int commentId);

    void create(UserCommentReaction userCommentReaction);

    void update(UserCommentReaction userCommentReaction);

    void delete(int userId, int commentId);
}

