package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.reactions.UserPostReaction;

public interface UserPostReactionRepository {
    UserPostReaction get(int userId, int postId);

    void create(UserPostReaction userPostReaction);

    void update(UserPostReaction userPostReaction);

    void delete(int userId, int postId);
}
