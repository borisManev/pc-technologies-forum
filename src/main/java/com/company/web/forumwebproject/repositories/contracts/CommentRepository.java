package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;

import java.util.List;
import java.util.Optional;

public interface CommentRepository {

    List<Comment> search(Optional<String> search);

    List<Comment> filter(CommentFilterOptions filterOptions);

    Comment getById(int id);

    void create(Comment comment);

    void update(Comment comment);

    void delete(int id);
}