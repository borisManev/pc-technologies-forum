package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserFilterOptions;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
//    List<User> filter(@RequestParam Optional<String> username, @RequestParam Optional<String> firstName,
//                      @RequestParam Optional<String> lastName, @RequestParam Optional<String> phoneNumber,
//                      @RequestParam Optional<String> email);

    User getById(int id);

    List<User> search(Optional<String> search);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhoneNumber(String phoneNumber);

    void create(User user);

    void update(User user);

    //void delete(int id);

    List<User> filter(UserFilterOptions userFilterOptions);

    List<User> getAll();
    public Long getNumberOfUsers();

//    void block(int id, Role role);
//
//    void unblock(int id, Role role);
}
