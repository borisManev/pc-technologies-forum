package main.java.com.company.web.forumwebproject.repositories;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Role", Role.class).list();
        }
    }

    @Override
    public Role getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Role role = session.get(Role.class, id);
            if (role == null) {
                throw new EntityNotFoundException("Role", id);
            }
            return role;
        }
    }

    @Override
    public Role getByRole(String roleName) {
        try (Session session = sessionFactory.openSession()) {
            List<Role> roles = session.createQuery("from Role where roleName = :role", Role.class)
                    .setParameter("role", roleName).list();
            if (roles.isEmpty()) {
                throw new EntityNotFoundException("Role", "name", roleName);
            }
            return roles.get(0);
        }
    }

    @Override
    public void create(Role role) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(role);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Role role) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(role);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Role role = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(role);
            session.getTransaction().commit();
        }
    }
}
