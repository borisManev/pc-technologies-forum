package main.java.com.company.web.forumwebproject.repositories.contracts;

import com.company.web.forumwebproject.models.tag.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();

    Tag getById(int id);

    Tag getByTitle(String title);

    void create(Tag tag);

    void update(Tag tag);

    void delete(int id);
}
