package main.java.com.company.web.forumwebproject.generateDbData;//package com.company.web.forumwebproject.generateDbData;
//
//import com.company.web.forumwebproject.models.post.Post;
//import com.company.web.forumwebproject.models.user.User;
//import com.company.web.forumwebproject.repositories.contracts.PostRepository;
//import com.company.web.forumwebproject.services.contracts.CommentService;
//import com.company.web.forumwebproject.services.contracts.PostService;
//import com.company.web.forumwebproject.services.contracts.UserService;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;
//import java.util.concurrent.ThreadLocalRandom;
//
//@RestController
//@RequestMapping("/generate")
//public class GenerateTestDataController {
//
//    private final UserService userService;
//    private final PostService postService;
//    private final CommentService commentService;
//    private final PostRepository postRepository;
//
//    @Autowired
//    public GenerateTestDataController(UserService userService, PostService postService, CommentService commentService, PostRepository postRepository) {
//        this.userService = userService;
//        this.postService = postService;
//        this.commentService = commentService;
//        this.postRepository = postRepository;
//    }
//
//    @PostMapping("/users")
//    public void generateUsers() throws JsonProcessingException {
//        String url = "https://api.parser.name/?api_key=5fcbf6449768bd626904ef06a32d3903&" +
//                "endpoint=generate&results=20&country_code=US";
//        RestTemplate restTemplate = new RestTemplate();
//        String userData = restTemplate.getForObject(url, String.class);
//        ObjectMapper mapper = new ObjectMapper();
//        JsonNode jsonNode = mapper.readTree(userData);
//
//
//        for (int i = 0; i < 20; i++) {
//            String firstName = jsonNode.get("data").get(i).get("name").get("firstname").get("name").toString();
//            firstName = firstName.substring(1, firstName.length() - 2);
//            if (firstName.length() < 4) continue;
//            String lastName = jsonNode.get("data").get(i).get("name").get("lastname").get("name").toString();
//            lastName = lastName.substring(1, lastName.length() - 2);
//            if (lastName.length() < 4) continue;
//            String email = jsonNode.get("data").get(i).get("email").toString();
//            email = email.substring(1, email.length() - 2);
//            String randomNumbersUsername = Integer.toString((int) Math.floor(Math.random() * 1000));
//            String username = firstName + lastName + randomNumbersUsername;
//            String randomNumbers = Integer.toString((int) Math.floor(Math.random() * 100000000));
//            String password = "";
//            password += firstName.charAt(0);
//            password += lastName.charAt(0);
//            password += randomNumbers;
//
//            User user = new User();
//            user.setFirstName(firstName);
//            user.setLastName(lastName);
//            user.setEmail(email);
//            user.setUsername(username);
//            user.setPassword(password);
//            userService.create(user);
//        }
//    }
//
//    @PostMapping("/postReactions")
//    public void generatePostReactions() {
//        for (int i = 0; i < 100; i++) {
//            postService.likePost(
//                    postService.getById(generateRandomInt(8,57)),userService.getById(generateRandomInt(78,228)));
//        }
//    }
//
//    @PostMapping("/commentReactions")
//    public void generateCommentReactions() {
//        for (int i = 0; i < 50; i++) {
//            commentService.dislikeComment(
//                    commentService.getById(generateRandomInt(18,67)),userService.getById(generateRandomInt(78,228)));
//        }
//    }
//
//    @PostMapping("/changePostOwners")
//    public void generatePostOwners() {
//        for (int i = 8; i < 58; i++) {
//            Post post = postService.getById(i);
//            post.setCreatedBy(userService.getById(generateRandomInt(78,228)));
//            postRepository.update(post);
//        }
//    }
//
//    public int generateRandomInt(int min,int max) {
//        return ThreadLocalRandom.current().nextInt(min, max);
//    }
//}