package main.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import com.company.web.forumwebproject.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.company.web.forumwebproject.utils.ValidationUtils.throwIfDuplicateField;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public Role getById(int id) {
        return roleRepository.getById(id);
    }

    @Override
    public Role getByRole(String title) {
        return roleRepository.getByRole(title);
    }

    @Override
    public void create(Role role) {
        throwIfDuplicateField(() -> roleRepository.getByRole(role.getRoleName()),
                String.format("Role with title %s already exists.", role.getRoleName()), role.getId());
        roleRepository.create(role);
    }

    @Override
    public void update(Role role) {
        throwIfDuplicateField(() -> roleRepository.getByRole(role.getRoleName()),
                String.format("Role with title %s already exists.", role.getRoleName()), role.getId());
        roleRepository.update(role);
    }

    @Override
    public void delete(int id) {
        roleRepository.delete(id);
    }
}
