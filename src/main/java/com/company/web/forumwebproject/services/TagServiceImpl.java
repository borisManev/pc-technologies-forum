package main.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.TagRepository;
import com.company.web.forumwebproject.services.contracts.TagService;
import com.company.web.forumwebproject.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.*;
import static com.company.web.forumwebproject.utils.ValidationUtils.throwIfDuplicateField;

@Service
public class TagServiceImpl implements TagService {

    public static final String TAG_WITH_TITLE_ALREADY_EXISTS = "Tag with title %s already exists.";
    private final TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tag> getAll() {
        return repository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Tag getByTitle(String title) {
        return repository.getByTitle(title);
    }

    @Override
    public void create(Tag tag) {
        throwIfDuplicateField(() -> repository.getByTitle(tag.getTitle()),
                String.format(TAG_WITH_TITLE_ALREADY_EXISTS, tag.getTitle()), tag.getId());
        validateTagTitle(tag.getTitle());
        repository.create(tag);
    }

    @Override
    public void update(Tag tag, User user) {
        checkModifyPermissions(user);
        ValidationUtils.throwIfDuplicateField(() -> repository.getByTitle(tag.getTitle()),
                String.format(TAG_WITH_TITLE_ALREADY_EXISTS, tag.getTitle()), tag.getId());
        repository.update(tag);
    }

    @Override
    public void delete(int id, User user) {
        checkModifyPermissions(user);
        repository.delete(id);
    }

    public void checkModifyPermissions(User user) {
        if (userIsDeleted(user) || userIsBlocked(user)) {
            throw new UnauthorizedOperationException(DELETED_OR_BLOCKED_ERROR_MESSAGE);
        }
        if (!userIsAdmin(user)) {
            throw new UnauthorizedOperationException(NOT_ADMIN_ERROR_MESSAGE);
        }
    }

    private void validateTagTitle(String title) {
        String TAG_TITLE_REGEX = "^[a-z0-9_-]+$";
        if (!title.matches(TAG_TITLE_REGEX)) {
            throw new InvalidParameterException(
                    "Tag title is invalid. Must only contain lowercase letters,numbers and underscore or dash.");
        }
    }
}
