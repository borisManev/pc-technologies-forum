package main.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.models.reactions.UserPostReaction;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.PostRepository;
import com.company.web.forumwebproject.repositories.contracts.UserPostReactionRepository;
import com.company.web.forumwebproject.services.contracts.PostService;
import com.company.web.forumwebproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.*;
import static java.lang.String.format;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository repository;
    private final TagService tagService;
    private final UserPostReactionRepository reactionRepository;

    @Autowired
    public PostServiceImpl(PostRepository repository, TagService tagService,
                           UserPostReactionRepository reactionRepository) {
        this.repository = repository;
        this.tagService = tagService;
        this.reactionRepository = reactionRepository;
    }

    @Override
    public List<Post> search(Optional<String> search) {
        return repository.search(search);
    }

    @Override
    public List<Post> filter(PostFilterOptions filterOptions) {
        return repository.filter(filterOptions);
    }

    @Override
    public Post getById(int id) {
        return repository.getById(id);
    }

    @Override
    public int getNumOfPosts() {
        return repository.getNumOfPosts();
    }

    @Override
    public Post create(Post post, User user) {
        post.setCreatedBy(user);
        checkModifyPermissions(post, user);
        LocalDateTime now = LocalDateTime.now();
        post.setCreation(now);
        post.setLastUpdate(now);
        return repository.create(post);
    }

    @Override
    public void update(Post post, User user) {
        post = fillFieldsFromDB(post);
        checkModifyPermissions(post, user);
        repository.update(post);
    }

    public Post fillFieldsFromDB(Post post) {
        Post postFromDB = repository.getById(post.getId());
        postFromDB.setTitle(post.getTitle());
        postFromDB.setContent(post.getContent());
        postFromDB.setLastUpdate(LocalDateTime.now());
        return postFromDB;
    }

    @Override
    public void delete(int id, User user) {
        Post postToDelete = getById(id);
        checkModifyPermissions(postToDelete, user);
        repository.delete(id);
    }

    @Override
    public void addTagToPost(Post post, Tag tag, User user) {
        checkModifyPermissions(post, user);

        Set<Tag> tags = post.getTags().isPresent() ? post.getTags().get() : new HashSet<>();

        if (tags.stream().anyMatch(t -> t.getTitle().equals(tag.getTitle()))) {
            throw new EntityDuplicationException(
                    format("Post with id %d already has tag with title %s.", post.getId(), tag.getTitle()));
        }

        Tag existingTag;
        try {
            existingTag = tagService.getByTitle(tag.getTitle());
        } catch (EntityNotFoundException e) {
            tagService.create(tag);
            existingTag = tag;
        }

        tags.add(existingTag);
        post.setTags(tags);
        repository.update(post);
    }

    @Override
    public void removeTagFromPost(Post post, int tagId, User user) {
        removeTag(post, tag -> tag.getId() == tagId, user,
                format("Post with id %d has no tag with id %d.", post.getId(), tagId));
    }

    @Override
    public void removeTagFromPost(Post post, String tagTitle, User user) {
        removeTag(post, tag -> tag.getTitle().equals(tagTitle), user,
                format("Post with id %d has no tag with title %s.", post.getId(), tagTitle));
    }

    private <T> void removeTag(Post post, Predicate<Tag> filter, User user, String message) {
        checkModifyPermissions(post, user);
        if (post.getTags().isPresent()) {
            Set<Tag> tags = post.getTags().get();
            Tag tag = tags.stream()
                    .filter(filter)
                    .findFirst()
                    .orElseThrow(() -> new InvalidParameterException(message));
            tags.remove(tag);
            post.setTags(tags);
            repository.update(post);
        } else {
            throw new InvalidParameterException(format("Post with id %d has no tags.", post.getId()));
        }
    }

    @Override
    public void likePost(Post post, User user) {
        likeOrDislikePost(post, user, true);
    }

    @Override
    public void dislikePost(Post post, User user) {
        likeOrDislikePost(post, user, false);
    }

    private void likeOrDislikePost(Post post, User user, boolean isLike) {
        UserPostReaction newReaction = new UserPostReaction(user.getId(), post.getId(), isLike);
        Set<UserPostReaction> set;

        if (post.getUserPostReactions().isPresent()) {
            set = post.getUserPostReactions().get();

            UserPostReaction currentReaction = set.stream()
                    .filter(upr -> upr.getUserId() == user.getId() && upr.getPostId() == post.getId())
                    .findFirst().orElse(null);

            if (currentReaction != null) {
                set.remove(currentReaction);
                if (currentReaction.isLiked() == isLike) {
                    reactionRepository.delete(user.getId(), post.getId());
                } else {
                    reactionRepository.update(newReaction);
                    set.add(newReaction);
                }
            } else {
                reactionRepository.create(newReaction);
                set.add(newReaction);
            }
        } else {
            set = new HashSet<>();
            set.add(newReaction);
            reactionRepository.create(newReaction);
        }
        post.setUserPostReactions(set);
    }

    public void checkModifyPermissions(Post post, User user) {
        if (userIsBlocked(user) || userIsDeleted(user)) {
            throw new UnauthorizedOperationException(DELETED_OR_BLOCKED_ERROR_MESSAGE);
        }
        if (!(userIsEntityCreator(post, user) || userIsAdmin(user))) {
            throw new UnauthorizedOperationException(MODIFY_ENTITY_ERROR_MESSAGE);
        }
    }
}
