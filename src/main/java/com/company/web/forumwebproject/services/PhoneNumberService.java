package main.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.PhoneRepository;
import com.company.web.forumwebproject.services.contracts.PhoneService;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.*;
import static com.company.web.forumwebproject.utils.ValidationUtils.throwIfDuplicateField;
import static java.lang.String.format;

@Service
public class PhoneNumberService implements PhoneService {
    public static final String PHONE_NUMBER_ALREADY_EXISTS = "Phone number already exists.";
    private final PhoneRepository phoneRepository;

    public PhoneNumberService(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    @Override
    public List<PhoneNumber> getAll() {
        return phoneRepository.getAll();
    }

    @Override
    public PhoneNumber getById(int id) {
        return phoneRepository.getById(id);
    }
    @Override
    public PhoneNumber getByNumber(String number) {
        return phoneRepository.getByNumber(number);
    }
    @Override
    public void create(PhoneNumber phoneNumber) {
        throwIfDuplicateField(() -> phoneRepository.getByNumber(phoneNumber.getPhoneNumber()),
                PHONE_NUMBER_ALREADY_EXISTS, phoneNumber.getId());
        validatePhoneNumber(phoneNumber.getPhoneNumber());
        phoneRepository.create(phoneNumber);
    }

    @Override
    public void update(PhoneNumber phoneNumber, User user) {
        checkModifyPermissions(user);
//        ValidationUtils.throwIfDuplicateField(() -> phoneRepository.getByNumber(phoneNumber.getPhoneNumber()),
//                PHONE_NUMBER_ALREADY_EXISTS, phoneNumber.getId());
        throwIfDuplicateNumberExists(phoneNumber.getPhoneNumber());
        phoneRepository.update(phoneNumber);
    }

    private void throwIfDuplicateNumberExists(String phoneNumber) {
        List<PhoneNumber> all = phoneRepository.getAll();
        if (all.stream().anyMatch(p->p.getPhoneNumber().equals(phoneNumber))){
            throw new EntityDuplicationException(
                    format("Phone number %s is already registered", phoneNumber));
        }
    }

    @Override
    public void delete(int id, User user) {
        checkModifyPermissions(user);
        phoneRepository.delete(id);
    }
    public void checkModifyPermissions(User user) {
        if (userIsDeleted(user) || userIsBlocked(user)) {
            throw new UnauthorizedOperationException(DELETED_OR_BLOCKED_ERROR_MESSAGE);
        }
        if (!userIsAdmin(user)) {
            throw new UnauthorizedOperationException(NOT_ADMIN_ERROR_MESSAGE);
        }
    }
    private void validatePhoneNumber(String number) {
        String PHONE_NUMBER_REGEX = "^[0-9]+$";
        if (!number.matches(PHONE_NUMBER_REGEX)) {
            throw new InvalidParameterException(
                    "Phone number is invalid. Must only contain numbers.");
        }
    }
}
