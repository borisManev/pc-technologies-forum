package main.java.com.company.web.forumwebproject.services.contracts;


import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserFilterOptions;

import java.util.List;
import java.util.Optional;



public interface UserService {
    //    List<User> filter(@RequestParam Optional<String> username, @RequestParam Optional<String> firstName,
//                      @RequestParam Optional<String> lastName, @RequestParam Optional<String> phoneNumber,
//                      @RequestParam Optional<String> email);
    List<User> search(Optional<String> search);

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhoneNumber(String phoneNumber);

    void create(User user);

    void update(User user, User use1);

    void delete(int id, User user);

    List<User> filter(UserFilterOptions userFilterOptions);

    void blockUser(int id, User user);

    public void makeUserAdmin(int id, User userPerformingAction);


    void unblockUser(int id, User user);

    List<User> getAll();
    void addPhoneToUser(User User, PhoneNumber phoneNumber);

    void removePhoneFromUser(User User,  int phoneId);

    void removePhoneFromUser(User User, String number);
    void removePhoneFromUser(User user) ;
    Long getNumberOfUsers();
}
