package main.java.com.company.web.forumwebproject.services.contracts;

import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.post.PostFilterOptions;
import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;

import java.util.List;
import java.util.Optional;

public interface PostService {
    List<Post> search(Optional<String> search);

    List<Post> filter(PostFilterOptions filterOptions);

    Post getById(int id);

    int getNumOfPosts();

    Post create(Post post, User user);

    void update(Post post, User user);

    void delete(int id, User user);

    void addTagToPost(Post post, Tag tag, User user);

    void removeTagFromPost(Post post, int tagId, User user);

    void removeTagFromPost(Post post, String tagTitle, User user);

    void likePost(Post post, User user);

    void dislikePost(Post post, User user);
}
