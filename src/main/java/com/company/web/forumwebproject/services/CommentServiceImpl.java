package main.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.reactions.UserCommentReaction;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.repositories.contracts.CommentRepository;
import com.company.web.forumwebproject.repositories.contracts.UserCommentReactionRepository;
import com.company.web.forumwebproject.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.*;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository repository;
    private final UserCommentReactionRepository reactionRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository repository, UserCommentReactionRepository reactionRepository) {
        this.repository = repository;
        this.reactionRepository = reactionRepository;
    }

    @Override
    public List<Comment> search(Optional<String> search) {
        return repository.search(search);
    }

    @Override
    public List<Comment> filter(CommentFilterOptions filterOptions) {
        return repository.filter(filterOptions);
    }

    @Override
    public Comment getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Post post, Comment comment, User user) {
        comment.setCreatedBy(user);
        checkModifyPermissions(comment, user);
        LocalDateTime now = LocalDateTime.now();
        comment.setCreation(now);
        comment.setLastUpdate(now);
        comment.setPostedOn(post);
        repository.create(comment);
    }

    @Override
    public void update(Comment comment, User user) {
        comment = fillFieldsFromDB(comment);
        checkModifyPermissions(comment, user);
        repository.update(comment);
    }

    public Comment fillFieldsFromDB(Comment comment) {
        Comment commentFromDB = getById(comment.getId());
        commentFromDB.setContent(comment.getContent());
        commentFromDB.setLastUpdate(LocalDateTime.now());
        return commentFromDB;
    }

    @Override
    public void delete(int id, User user) {
        Comment comment = repository.getById(id);
        checkModifyPermissions(comment, user);
        repository.delete(id);
    }

    @Override
    public void likeComment(Comment comment, User user) {
        likeOrDislikeComment(comment, user, true);
    }

    @Override
    public void dislikeComment(Comment comment, User user) {
        likeOrDislikeComment(comment, user, false);
    }

    private void likeOrDislikeComment(Comment comment, User user, boolean isLike) {
        UserCommentReaction newReaction = new UserCommentReaction(user.getId(), comment.getId(), isLike);
        Set<UserCommentReaction> set;

        if (comment.getUserCommentReactions().isPresent()) {
            set = comment.getUserCommentReactions().get();

            UserCommentReaction currentReaction = set.stream()
                    .filter(upr -> upr.getUserId() == user.getId() && upr.getCommentId() == comment.getId())
                    .findFirst().orElse(null);

            if (currentReaction != null) {
                set.remove(currentReaction);
                if (currentReaction.isLiked() == isLike) {
                    reactionRepository.delete(user.getId(), comment.getId());
                } else {
                    reactionRepository.update(newReaction);
                    set.add(newReaction);
                }
            } else {
                reactionRepository.create(newReaction);
                set.add(newReaction);
            }
        } else {
            set = new HashSet<>();
            set.add(newReaction);
            reactionRepository.create(newReaction);
        }
        comment.setUserCommentReactions(set);
    }

    public void checkModifyPermissions(Comment comment, User user) {
        if (userIsBlocked(user) || userIsDeleted(user)) {
            throw new UnauthorizedOperationException(DELETED_OR_BLOCKED_ERROR_MESSAGE);
        }
        if (!(userIsEntityCreator(comment, user) || userIsAdmin(user))) {
            throw new UnauthorizedOperationException(MODIFY_ENTITY_ERROR_MESSAGE);
        }
    }
}


