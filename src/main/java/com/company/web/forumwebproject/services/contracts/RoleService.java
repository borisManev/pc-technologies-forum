package main.java.com.company.web.forumwebproject.services.contracts;

import com.company.web.forumwebproject.models.role.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAll();

    Role getById(int id);

    Role getByRole(String title);

    void create(Role role);

    void update(Role role);

    void delete(int id);
}
