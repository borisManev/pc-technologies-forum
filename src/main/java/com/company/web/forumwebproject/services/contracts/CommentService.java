package main.java.com.company.web.forumwebproject.services.contracts;

import com.company.web.forumwebproject.models.comment.Comment;
import com.company.web.forumwebproject.models.comment.CommentFilterOptions;
import com.company.web.forumwebproject.models.post.Post;
import com.company.web.forumwebproject.models.user.User;

import java.util.List;
import java.util.Optional;

public interface CommentService {

    List<Comment> search(Optional<String> search);

    List<Comment> filter(CommentFilterOptions filterOptions);

    Comment getById(int id);

    void create(Post post, Comment comment, User user);

    void update(Comment comment, User user);

    void delete(int id, User user);

    void likeComment(Comment comment, User user);

    void dislikeComment(Comment comment, User user);
}
