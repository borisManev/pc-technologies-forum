package main.java.com.company.web.forumwebproject.services.contracts;

import com.company.web.forumwebproject.models.tag.Tag;
import com.company.web.forumwebproject.models.user.User;

import java.util.List;

public interface TagService {
    List<Tag> getAll();

    Tag getById(int id);

    Tag getByTitle(String title);

    void create(Tag tag);

    void update(Tag tag, User user);

    void delete(int id, User user);
}
