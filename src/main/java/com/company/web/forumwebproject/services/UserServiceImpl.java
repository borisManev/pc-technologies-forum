package main.java.com.company.web.forumwebproject.services;

import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.exceptions.InvalidParameterException;
import com.company.web.forumwebproject.exceptions.UnauthorizedOperationException;
import com.company.web.forumwebproject.models.role.Role;
import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.User;
import com.company.web.forumwebproject.models.user.UserFilterOptions;
import com.company.web.forumwebproject.repositories.contracts.RoleRepository;
import com.company.web.forumwebproject.repositories.contracts.UserRepository;
import com.company.web.forumwebproject.services.contracts.PhoneService;
import com.company.web.forumwebproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static com.company.web.forumwebproject.utils.AuthorizationUtils.*;
import static com.company.web.forumwebproject.utils.ValidationUtils.throwIfDuplicateField;
import static java.lang.String.format;

@Service
public class UserServiceImpl implements UserService {
    public static final String USER_WITH_USERNAME_ALREADY_EXISTS = "User with username %s already exists.";
    public static final String USER_WITH_EMAIL_ALREADY_EXISTS = "User with email %s already exists.";
    public static final String USER_IS_NOT_BLOCKED = "This user is not blocked.";
    public static final String USER_IS_ALREADY_BLOCKED_OR_DELETED = "This user is already blocked or deleted.";
    public static final String USER_IS_ALREADY_ADMIN = "This user is already admin.";
    public static final String USER_IS_ALREADY_DELETED = "This user is already deleted";
    public static final String CANNOT_MODIFY_DELETED_USER = "Cannot modify user.User is deleted.";
    public static final String USER_DELETED = "User has been deleted.";
    public static final String DELETED = "deleted"; //reserved username for deleted users
    public static int deletedUsers = 1; //reserved username for deleted users
    public static final String USERNAME_DELETED_IS_INVALID = "Username " + DELETED + " cannot be used.";

    private final RoleRepository roleRepository;

    private final UserRepository repository;
    private final PhoneService phoneService;

    @Autowired
    public UserServiceImpl(RoleRepository roleRepository, UserRepository repository, PhoneService phoneService) {
        this.roleRepository = roleRepository;
        this.repository = repository;
        this.phoneService = phoneService;
    }


    @Override
    public List<User> search(Optional<String> search) {
        return repository.search(search);
    }

    @Override
    public User getById(int id) {
//        return throwIfUserDeleted(repository.getById(id));
        return repository.getById(id);
    }


    public User getByUsername(String username) {
        throwIfUsernameIsDeleted(username);
        return throwIfUserDeleted(repository.getByUsername(username));
    }

    @Override
    public User getByEmail(String email) {
        return throwIfUserDeleted(repository.getByEmail(email));
    }

    @Override
    public User getByPhoneNumber(String phoneNumber) {
        return throwIfUserDeleted(repository.getByPhoneNumber(phoneNumber));
    }

    private User throwIfUserDeleted(User user) {
        if (userIsDeleted(user)) {
            throw new UnauthorizedOperationException(USER_DELETED);
        }
        return user;
    }

    private void throwIfUsernameIsDeleted(String username) {
        if (username.equalsIgnoreCase(DELETED)) {
            throw new UnsupportedOperationException(USERNAME_DELETED_IS_INVALID);
        }
    }

    @Override
    public void create(User user) {
        throwIfDuplicateField(() -> repository.getByUsername(user.getUsername()),
                String.format(USER_WITH_USERNAME_ALREADY_EXISTS, user.getUsername()), user.getId());
        throwIfDuplicateField(() -> repository.getByEmail(user.getEmail()),
                String.format(USER_WITH_EMAIL_ALREADY_EXISTS, user.getEmail()), user.getId());

        throwIfUsernameIsDeleted(user.getUsername());
        LocalDateTime now = LocalDateTime.now();
        user.setCreation(now);
        user.setLastLogin(now);

        //This code was used when inserting mock users from an external API.
        // Do not remove as it may be used in the future as well.
//        LocalDateTime creation = generateRandomDate(
//                LocalDateTime.of(2010, 1, 1,0,0,0));
//        user.setCreation(creation);
//        LocalDateTime lastLogin = generateRandomDate(creation);
//        user.setLastLogin(lastLogin);


        List<Role> roles = new ArrayList<>();
        roles.add(roleRepository.getByRole("general-user"));
        user.setRoles(roles);

        repository.create(user);
    }

    //This code was used when inserting mock users from an external API.
    // Do not remove as it may be used in the future as well.
//    public LocalDateTime generateRandomDate(LocalDateTime start) {
//        long minDay = start.toEpochSecond(ZoneOffset.UTC);
//        long maxDay = LocalDateTime.now()
//                .toEpochSecond(ZoneOffset.UTC);
//        long randomSecond = ThreadLocalRandom.current().nextLong(minDay, maxDay);
//        int randomNanosecond = ThreadLocalRandom.current().nextInt(0,999999999);
//        return LocalDateTime.ofEpochSecond(randomSecond,randomNanosecond, ZoneOffset.UTC);
//    }

    @Override
    public void update(User user, User userPerformingAction) {
        checkModifyPermissions(user, userPerformingAction);

        User finalUser = user;
        throwIfDuplicateField(() -> repository.getByEmail(finalUser.getEmail()),
                String.format(USER_WITH_EMAIL_ALREADY_EXISTS, user.getEmail()), user.getId());

        user = fillFields(user);

        if (userIsDeleted(user)) {
            throw new UnsupportedOperationException(CANNOT_MODIFY_DELETED_USER);
        }
        repository.update(user);
    }


    private User fillFields(User user) {
        User userFromDb = repository.getById(user.getId());
        userFromDb.setEmail(user.getEmail());
        userFromDb.setFirstName(user.getFirstName());
        userFromDb.setLastName(user.getLastName());
        userFromDb.setPassword(user.getPassword());
        userFromDb.setLastLogin(user.getLastLogin());
        userFromDb.setPhoneNumber(user.getPhoneNumber().orElse(null));

        return userFromDb;
    }


    @Override
    public void delete(int id, User userPerformingAction) {
        User userToBeDeleted = getById(id);
        checkModifyPermissions(userToBeDeleted, userPerformingAction);

        if (userIsDeleted(userToBeDeleted)) {
            throw new UnsupportedOperationException(USER_IS_ALREADY_BLOCKED_OR_DELETED);
        }

//        int byteSize = userToBeDeleted.getUsername().hashCode();
//        String hashStr = String.valueOf(byteSize);
//        String firstFive = hashStr.substring(0, 5);

        userToBeDeleted.setUsername(DELETED + userToBeDeleted.getId());
        deletedUsers++;
        List<Role> roles = userToBeDeleted.getRoles();
        roles.add(roleRepository.getByRole("deleted"));
        userToBeDeleted.setRoles(roles);
        repository.update(userToBeDeleted);
    }

    @Override
    public List<User> filter(UserFilterOptions userFilterOptions) {
        return repository.filter(userFilterOptions);
    }

    @Override
    public void blockUser(int id, User userPerformingAction) {
        User userToBeBlocked = getById(id);
        checkModifyPermissions(userPerformingAction);
        if (userIsBlocked(userToBeBlocked)) {
            throw new UnsupportedOperationException(USER_IS_ALREADY_BLOCKED_OR_DELETED);
        }

        List<Role> roles = userToBeBlocked.getRoles();
        Role role = roleRepository.getByRole("blocked");
        roles.add(role);
        if (roles.stream().anyMatch(r -> r.getRoleName().equals("admin"))) {
            Role role1 = roleRepository.getByRole("admin");
            roles.remove(role1);

        }
        userToBeBlocked.setRoles(roles);
        repository.update(userToBeBlocked);
    }

    @Override
    public void unblockUser(int id, User userPerformingAction) {
        User userToBeUnblocked = getById(id);
        checkModifyPermissions(userPerformingAction);
        if (!userIsBlocked(userToBeUnblocked)) {
            throw new UnsupportedOperationException(USER_IS_NOT_BLOCKED);
        }

        List<Role> roles = userToBeUnblocked.getRoles();
        Role blocked = roles.stream()
                .filter(role -> "blocked".equals(role.getRoleName()))
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException(USER_IS_NOT_BLOCKED));
        roles.remove(blocked);

        userToBeUnblocked.setRoles(roles);
        repository.update(userToBeUnblocked);
    }

    @Override
    public void makeUserAdmin(int id, User userPerformingAction) {
        User userToBeMadeAdmin = getById(id);
        checkModifyPermissions(userPerformingAction);
        if (userIsAdmin(userToBeMadeAdmin)) {
            throw new UnsupportedOperationException(USER_IS_ALREADY_ADMIN);
        }

        List<Role> roles = userToBeMadeAdmin.getRoles();
        Role role = roleRepository.getByRole("admin");
        roles.add(role);
        userToBeMadeAdmin.setRoles(roles);
        repository.update(userToBeMadeAdmin);
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }


    private void checkModifyPermissions(User user, User userPerformingAction) {
        if (userIsDeleted(userPerformingAction) || userIsBlocked(userPerformingAction)) {
            throw new UnauthorizedOperationException(DELETED_OR_BLOCKED_ERROR_MESSAGE);
        }
        if (!(userIsAdmin(userPerformingAction) || user.equals(userPerformingAction))) {
            throw new UnauthorizedOperationException(MODIFY_ENTITY_ERROR_MESSAGE);
        }
    }

    private void checkModifyPermissions(User userPerformingAction) {
        if (userIsDeleted(userPerformingAction) || userIsBlocked(userPerformingAction)) {
            throw new UnauthorizedOperationException(DELETED_OR_BLOCKED_ERROR_MESSAGE);
        }
        if (!userIsAdmin(userPerformingAction)) {
            throw new UnauthorizedOperationException(NOT_ADMIN_ERROR_MESSAGE);
        }
    }

    @Override
    public void addPhoneToUser(User user, PhoneNumber phoneNumber) {
        checkModifyPermissions(user);
        PhoneNumber phoneNumbers;
        PhoneNumber existingNumber;
        try {
            existingNumber = phoneService.getByNumber(phoneNumber.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            phoneService.create(phoneNumber);
            existingNumber = phoneNumber;
        }

        phoneNumbers = existingNumber;
        user.setPhoneNumber(phoneNumbers);
        repository.update(user);
    }

    @Override
    public void removePhoneFromUser(User user, int phoneId) {
        removeNumber(user, number -> number.getId() == phoneId,
                format("User with id %d has no tag with id %d.", user.getId(), phoneId));
    }

    @Override
    public void removePhoneFromUser(User user, String number) {
        removeNumber(user, numbers -> numbers.getPhoneNumber().equals(number),
                format("User with id %d has no phone number %s.", user.getId(), number));

    }

    private <T> void removeNumber(User user, Predicate<PhoneNumber> filter, String message) {
        checkModifyPermissions(user);
        if (user.getPhoneNumber().isPresent()) {
            user.setPhoneNumber(null);
            repository.update(user);
        } else {
            throw new InvalidParameterException(format("User with id %d has no phone number.", user.getId()));
        }
    }

    @Override
    public void removePhoneFromUser(User user) {
        if (user.getPhoneNumber().isPresent()) {
            user.setPhoneNumber(null);
            repository.update(user);
        } else {
            throw new InvalidParameterException(format("User with id %d has no phone number.", user.getId()));
        }
    }

    @Override
    public Long getNumberOfUsers() {
        return repository.getNumberOfUsers();
    }
}
