package main.java.com.company.web.forumwebproject.services.contracts;

import com.company.web.forumwebproject.models.user.PhoneNumber;
import com.company.web.forumwebproject.models.user.User;

import java.util.List;

public interface PhoneService {
    List<PhoneNumber> getAll();

    PhoneNumber getById(int id);

    PhoneNumber getByNumber(String number);

    void create(PhoneNumber phoneNumber);

    void update(PhoneNumber phoneNumber, User user);

    void delete(int id, User user);
}
