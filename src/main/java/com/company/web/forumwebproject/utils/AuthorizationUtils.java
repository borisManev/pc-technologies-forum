package main.java.com.company.web.forumwebproject.utils;

import com.company.web.forumwebproject.models.contracts.Creatable;
import com.company.web.forumwebproject.models.user.User;

public class AuthorizationUtils {

    public static final String DELETED_OR_BLOCKED_ERROR_MESSAGE = "Deleted or blocked users can not modify a entity.";
    public static final String MODIFY_ENTITY_ERROR_MESSAGE = "Only admin or entity creator can modify a entity.";
    public static final String NOT_ADMIN_ERROR_MESSAGE = "Only admins can perform this action.";

    public static boolean userIsBlocked(User user) {
        return user.getRoles().stream().anyMatch(r -> r.getRoleName().equalsIgnoreCase("blocked"));
    }

    public static boolean userIsDeleted(User user) {
        return user.getRoles().stream().anyMatch(r -> r.getRoleName().equalsIgnoreCase("deleted"));
    }

    public static <T extends Creatable> boolean userIsEntityCreator(T obj, User user) {
        return obj.getCreatedBy().equals(user);
    }

    public static boolean userIsAdmin(User user) {
        return user.getRoles().stream().anyMatch(r -> r.getRoleName().equalsIgnoreCase("admin"));
    }
}
