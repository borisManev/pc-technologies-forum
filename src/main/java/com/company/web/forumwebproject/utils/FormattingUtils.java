package main.java.com.company.web.forumwebproject.utils;

import java.time.format.DateTimeFormatter;

public class FormattingUtils {

    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("LLLL dd,yyyy");
}
