package main.java.com.company.web.forumwebproject.utils;

import com.company.web.forumwebproject.exceptions.EntityDuplicationException;
import com.company.web.forumwebproject.exceptions.EntityNotFoundException;
import com.company.web.forumwebproject.models.contracts.Identifiable;

import java.util.function.Supplier;

public class ValidationUtils {

    public static <T extends Identifiable> void throwIfDuplicateField(Supplier<T> getObj, String message, int objId)
            throws EntityDuplicationException {
        boolean flag = true;
        try {
            T obj = getObj.get();
            if (obj.getId() == objId) {
                flag = false;
            }
        } catch (EntityNotFoundException e) {
            flag = false;
        }
        if (flag) {
            throw new EntityDuplicationException(message);
        }
    }
}
