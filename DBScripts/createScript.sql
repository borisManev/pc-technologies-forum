-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.11.1-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


-- Dumping database structure for forumdb
CREATE DATABASE IF NOT EXISTS `forumdb` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci */;
USE `forumdb`;

-- Dumping structure for table forumdb.comments
CREATE TABLE IF NOT EXISTS `comments`
(
    `comment_id`  int(11)       NOT NULL AUTO_INCREMENT,
    `content`     varchar(1024) NOT NULL,
    `creation`    datetime      NOT NULL,
    `last_update` datetime      NOT NULL,
    `user_id`     int(11)       NOT NULL,
    `post_id`     int(11)       NOT NULL,
    PRIMARY KEY (`comment_id`),
    KEY `comments_posts_postID_fk` (`post_id`),
    KEY `comments_users_userID_fk` (`user_id`),
    CONSTRAINT `comments_posts_postID_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
    CONSTRAINT `comments_users_userID_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `check_content_length` CHECK (octet_length(`content`) > 4)
) ENGINE = InnoDB
  AUTO_INCREMENT = 68
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.phone_numbers
CREATE TABLE IF NOT EXISTS `phone_numbers`
(
    `user_id`      int(11)     NOT NULL,
    `phone_number` varchar(15) NOT NULL,
    PRIMARY KEY (`user_id`),
    CONSTRAINT `phone_numbers_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.posts
CREATE TABLE IF NOT EXISTS `posts`
(
    `post_id`     int(11)       NOT NULL AUTO_INCREMENT,
    `title`       varchar(64)   NOT NULL,
    `content`     varchar(8192) NOT NULL,
    `creation`    datetime      NOT NULL,
    `last_update` datetime      NOT NULL,
    `user_id`     int(11)       NOT NULL,
    PRIMARY KEY (`post_id`),
    KEY `posts_users_userID_fk` (`user_id`),
    CONSTRAINT `posts_users_userID_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `check_content_length` CHECK (octet_length(`content`) >= 32),
    CONSTRAINT `check_title_length` CHECK (octet_length(`title`) >= 16)
) ENGINE = InnoDB
  AUTO_INCREMENT = 58
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.posts_tags
CREATE TABLE IF NOT EXISTS `posts_tags`
(
    `post_id` int(11) NOT NULL,
    `tag_id`  int(11) NOT NULL,
    PRIMARY KEY (`post_id`, `tag_id`),
    KEY `posts_tags_chain_tags_tagID_fk` (`tag_id`),
    CONSTRAINT `posts_tags_chain_posts_postID_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
    CONSTRAINT `posts_tags_chain_tags_tagID_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.roles
CREATE TABLE IF NOT EXISTS `roles`
(
    `role_id`   smallint(6) NOT NULL AUTO_INCREMENT,
    `role_name` varchar(16) NOT NULL,
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `roles_pk` (`role_name`),
    CONSTRAINT `check_role_name_length` CHECK (octet_length(`role_name`) >= 4)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.tags
CREATE TABLE IF NOT EXISTS `tags`
(
    `tag_id`      int(11)     NOT NULL AUTO_INCREMENT,
    `title`       varchar(16) NOT NULL,
    `description` varchar(64) DEFAULT NULL,
    PRIMARY KEY (`tag_id`),
    UNIQUE KEY `tags_pk` (`title`),
    CONSTRAINT `check_description` CHECK (octet_length(`description`) >= 8),
    CONSTRAINT `check_title_length` CHECK (octet_length(`title`) >= 4)
) ENGINE = InnoDB
  AUTO_INCREMENT = 37
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.users
CREATE TABLE IF NOT EXISTS `users`
(
    `user_id`    int(11)      NOT NULL AUTO_INCREMENT,
    `first_name` varchar(32)  NOT NULL,
    `last_name`  varchar(32)  NOT NULL,
    `username`   varchar(32)  NOT NULL,
    `password`   char(128)    NOT NULL,
    `email`      varchar(254) NOT NULL,
    `creation`   datetime     NOT NULL,
    `last_login` datetime     NOT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `users_pk2` (`username`),
    UNIQUE KEY `users_pk3` (`email`),
    CONSTRAINT `check_first_name_length` CHECK (octet_length(`first_name`) >= 4),
    CONSTRAINT `check_last_name_length` CHECK (octet_length(`last_name`) >= 4),
    CONSTRAINT `check_username_length` CHECK (octet_length(`username`) >= 4)
) ENGINE = InnoDB
  AUTO_INCREMENT = 229
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.users_comments_reactions
CREATE TABLE IF NOT EXISTS `users_comments_reactions`
(
    `user_id`    int(11)    NOT NULL,
    `comment_id` int(11)    NOT NULL,
    `is_liked`   tinyint(1) NOT NULL,
    PRIMARY KEY (`user_id`, `comment_id`),
    KEY `users_comments_reactions_comments_commentID_fk` (`comment_id`),
    CONSTRAINT `users_comments_reactions_comments_commentID_fk` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`comment_id`),
    CONSTRAINT `users_comments_reactions_users_userID_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.users_posts_reactions
CREATE TABLE IF NOT EXISTS `users_posts_reactions`
(
    `user_id`  int(11)    NOT NULL,
    `post_id`  int(11)    NOT NULL,
    `is_liked` tinyint(1) NOT NULL,
    PRIMARY KEY (`user_id`, `post_id`),
    KEY `users_posts_reactions_posts_postID_fk` (`post_id`),
    CONSTRAINT `users_posts_reactions_posts_postID_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
    CONSTRAINT `users_posts_reactions_users_userID_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for table forumdb.users_roles
CREATE TABLE IF NOT EXISTS `users_roles`
(
    `user_id` int(11)     NOT NULL,
    `role_id` smallint(6) NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`),
    KEY `users_roles_roles_role_id_fk` (`role_id`),
    CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
    CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  COLLATE = latin1_swedish_ci;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES = IFNULL(@OLD_SQL_NOTES, 1) */;
